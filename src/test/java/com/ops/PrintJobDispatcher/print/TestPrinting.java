package com.ops.PrintJobDispatcher.print;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PDFRenderer;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import javax.print.attribute.Attribute;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.Media;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.PrinterName;
import java.awt.*;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

@Disabled
public class TestPrinting {

    @Test
    public void testLookupByName() {
        AttributeSet aset = new HashPrintServiceAttributeSet();
        aset.add(new PrinterName("PR0101_PJD", null));
        PrintService[] pss = javax.print.PrintServiceLookup.lookupPrintServices(DocFlavor.SERVICE_FORMATTED.PRINTABLE, aset);
        for (PrintService ps : pss) {
            System.err.println("Printer: " + ps);
        }
    }

    @Test
    public void testTraySource() {
        PrintService[] pss = javax.print.PrintServiceLookup.lookupPrintServices(null, null);
        for (PrintService ps : pss) {
            System.err.println("Printer: " + ps);
            Class<?>[] supAttrs = ps.getSupportedAttributeCategories();
            for (Class<?> supAttr : supAttrs) {
                System.err.println("SAC:" + supAttr);
            }

            PrintServiceAttributeSet set = ps.getAttributes();
            for (Attribute attr : set.toArray()) {
                System.err.println("ATT:" + attr.getClass().getName() + "=" + attr);
            }

            Media[] medias = (Media[]) ps.getSupportedAttributeValues(Media.class, null, null);
            for (Media media : medias) {
                System.err.println("MEDIA:" + media.getClass().getName() + "=" + media);
            }

            DocFlavor[] docFlavors = ps.getSupportedDocFlavors();
            for (DocFlavor docFlavor : docFlavors) {
                System.err.println("DOCV:" + docFlavor);
            }
        }
    }

    @Test
    public void testPrintPs() throws FileNotFoundException, PrintException {
        PrintService[] pss = javax.print.PrintServiceLookup.lookupPrintServices(null, null);
        for (PrintService ps : pss) {
            // if (ps.getName().equals("HP LaserJet 4200 PCL 6"))
            if (ps.getName().equals("PR0101_PJD")) {
                DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
                PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
                aset.add(MediaSizeName.ISO_A4);

                FileInputStream fis = new FileInputStream("c:\\opsj\\spool\\1247743127-000000\\data.ps");
                Doc doc = new SimpleDoc(fis, flavor, null);

                DocPrintJob job = ps.createPrintJob();
                job.print(doc, aset);
            }
        }
    }

    @Test
    public void testPrintDialog() {
        PrinterJob.getPrinterJob().printDialog();
    }

    @Test
    public void testPrintPdfAsPs() throws IOException, PrintException, InterruptedException {
        // gswin32c -dNOPAUSE -dBATCH -dSAFER -sDEVICE=pswrite -sOutputFile=- data.pdf

        String[] cmd = {"gswin32c", "-q", "-dNOPAUSE", "-dBATCH", "-dSAFER", "-sDEVICE=pswrite", "-sOutputFile=-", "c:\\opsj\\spool\\1247743127-000000\\data.pdf"};

        Process process = Runtime.getRuntime().exec(cmd);
        InputStream pdfStream = process.getInputStream();

        PrintService[] pss = javax.print.PrintServiceLookup.lookupPrintServices(null, null);
        for (PrintService ps : pss) {
            if (ps.getName().equals("HP LaserJet 4200 PCL 6"))
            // if (ps.getName().equals("PR0101_PJD"))
            {
                Media mediaTray = null;
                Media[] medias = (Media[]) ps.getSupportedAttributeValues(Media.class, null, null);
                for (Media media : medias) {
                    System.err.println("MEDIA:" + media.getClass().getName() + "=" + media);
                    if (media.toString().contains("Vorgelocht")) {
                        System.err.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");

                        mediaTray = media;
                    }
                }

                DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
                PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
                aset.add(MediaSizeName.ISO_A4);
                aset.add(mediaTray);
                // aset.add(new Destination(new URI("file:/c:/opsj/spool/out.prn")));

                // FileInputStream pdfStream = new FileInputStream("c:\\opsj\\spool\\1247743127-000000\\data.xxx");
                Doc doc = new SimpleDoc(pdfStream, flavor, null);

                DocPrintJob job = ps.createPrintJob();
                job.print(doc, aset);
            }
        }

        int processRet = process.waitFor();
        System.err.println(processRet);
    }

    @Test
    public void testPrintPdfViaRenderer() throws IOException, PrintException {
        File file = new File("c:\\opsj\\spool\\1247743127-000000\\data.pdf");

// set up the PDF reading
        RandomAccessFile raf = new RandomAccessFile(file, "r");
        FileChannel channel = raf.getChannel();
        ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
        final PDFFile pdffile = new PDFFile(buf);

        PrintService[] pss = javax.print.PrintServiceLookup.lookupPrintServices(null, null);
        for (PrintService ps : pss) {
            if (ps.getName().equals("HP LaserJet 4200 PCL 6"))
            // if (ps.getName().equals("PR0101_PJD"))
            {
                Media mediaTray = null;
                Media[] medias = (Media[]) ps.getSupportedAttributeValues(Media.class, null, null);
                for (Media media : medias) {
                    System.err.println("MEDIA:" + media.getClass().getName() + "=" + media);
                    if (media.toString().contains(" Fach 2")) {
                        System.err.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");

                        mediaTray = media;
                    }
                }

                DocFlavor flavor = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
                PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
                aset.add(MediaSizeName.ISO_A4);
                aset.add(mediaTray);
                aset.add(new Copies(2));
                aset.add(new JobName("marios funny java print tests", null));

                DocPrintJob printJob = ps.createPrintJob();

                Printable printable = (graphics, format, pageIndex) -> {
                    if (pageIndex >= 0 && pageIndex < pdffile.getNumPages()) {
                        Graphics2D g2 = (Graphics2D) graphics;
                        PDFPage page = pdffile.getPage(pageIndex + 1);

                        // we assume that the print job is already within the right bounds
                        Rectangle imgbounds = new Rectangle(0, 0, (int) page.getWidth(), (int) page.getHeight());

                        PDFRenderer pgs = new PDFRenderer(page, g2, imgbounds, null, null);
                        try {
                            page.waitForFinish();
                            pgs.run();
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }

                        return Printable.PAGE_EXISTS;
                    } else {
                        return Printable.NO_SUCH_PAGE;
                    }
                };
                Doc doc = new SimpleDoc(printable, flavor, null);

                printJob.print(doc, aset);
            }
        }
    }
}
