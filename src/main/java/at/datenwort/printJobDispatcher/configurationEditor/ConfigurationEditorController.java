/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.configurationEditor;

import at.datenwort.printJobDispatcher.lib.EnvironmentAccess;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class ConfigurationEditorController implements Initializable {
    @FXML
    private VBox root;

    @FXML
    private Button refreshButton;

    @FXML
    private Button importButton;

    @FXML
    private Button pathNewButton;

    @FXML
    private Button pathDeleteButton;

    @FXML
    private Button pathCopyButton;

    @FXML
    private Button pathRenameButton;

    @FXML
    private TreeView<ConfigTreeItem> tree;

    @FXML
    private Button entryNewButton;

    @FXML
    private Button entryDeleteButton;

    @FXML
    private Button entryCopyButton;

    @FXML
    private TableView<ConfigEntry> entries;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tree.setShowRoot(true);
        tree.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        tree.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
        {
            if (newValue == null || newValue.getValue() == null) {
                entries.getItems().clear();
            } else {
                populateEntries(newValue.getValue());
            }
        });

        TableColumn<ConfigEntry, String> keyCol = new TableColumn<>("key");
        keyCol.setCellFactory(TextFieldTableCell.forTableColumn());
        keyCol.setCellValueFactory(new PropertyValueFactory<>("key"));
        keyCol.prefWidthProperty().bind(entries.widthProperty().multiply(0.3));
        keyCol.setEditable(true);

        TableColumn<ConfigEntry, String> valueCol = new TableColumn<>("value");
        valueCol.setCellFactory(TextFieldTableCell.forTableColumn());
        valueCol.setCellValueFactory(new PropertyValueFactory<>("value"));
        valueCol.prefWidthProperty().bind(entries.widthProperty().multiply(0.7));
        valueCol.setEditable(true);

        entries.getColumns().setAll(keyCol, valueCol);
        entries.setEditable(true);

        refreshButton.setOnAction(evt ->
                populateTree());

        importButton.setOnAction(evt ->
        {

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose configuration");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("oreg-Files", "*.oreg"));
            File chosenConfiguration = fileChooser.showOpenDialog(importButton.getScene().getWindow());
            if (chosenConfiguration != null) {
                importRegistry(chosenConfiguration);
            }
        });

        pathNewButton.setOnAction(evt ->
        {
            TreeItem<ConfigTreeItem> treeItem = tree.getSelectionModel().getSelectedItem();
            if (treeItem == null || treeItem.getValue() == null) {
                return;
            }

            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("new path");
            dialog.setHeaderText("enter new path name");
            dialog.showAndWait().ifPresent(newPath ->
            {
                ConfigTreeItem parentEntry = treeItem.getValue();

                ConfigTreeItem newPathEntry = new ConfigTreeItem(
                        parentEntry.getConfigNode().node(newPath),
                        newPath);
                treeItem.getChildren().add(0, new TreeItem<>(newPathEntry));
                treeItem.setExpanded(true);
            });
        });

        pathDeleteButton.setOnAction(evt ->
        {
            TreeItem<ConfigTreeItem> treeItem = tree.getSelectionModel().getSelectedItem();
            if (treeItem == null || treeItem.getValue() == null) {
                return;
            }

            try {
                treeItem.getValue().getConfigNode().removeNode();
            } catch (BackingStoreException e) {
                MessageDialog.showError(e);
            }
            treeItem.getParent().getChildren().remove(treeItem);
        });

        pathCopyButton.setOnAction(evt ->
        {
            TreeItem<ConfigTreeItem> treeItem = tree.getSelectionModel().getSelectedItem();
            if (treeItem == null || treeItem.getValue() == null) {
                return;
            }

            try {
                String newName = findUniqueNodeName(
                        treeItem.getValue().getConfigNode().parent(),
                        treeItem.getValue().getChildName());
                Preferences newPreferences = copyNode(treeItem.getValue().getConfigNode(),
                        treeItem.getValue().getConfigNode().parent().node(newName));

                populateTreeNode(treeItem.getParent(), newPreferences);
            } catch (BackingStoreException e) {
                MessageDialog.showError(e);
            }
        });

        pathRenameButton.setOnAction(evt ->
        {
            TreeItem<ConfigTreeItem> treeItem = tree.getSelectionModel().getSelectedItem();
            if (treeItem == null || treeItem.getValue() == null) {
                return;
            }

            TextInputDialog dialog = new TextInputDialog(treeItem.getValue().getConfigNode().name());
            dialog.setTitle("rename path");
            dialog.setHeaderText("enter new path name");
            dialog.showAndWait().ifPresent(newPath ->
            {
                try {
                    Preferences newPreferences = copyNode(treeItem.getValue().getConfigNode(),
                            treeItem.getValue().getConfigNode().parent().node(newPath));

                    populateTreeNode(treeItem.getParent(), newPreferences);

                    treeItem.getValue().getConfigNode().removeNode();
                    treeItem.getParent().getChildren().remove(treeItem);
                } catch (BackingStoreException e) {
                    MessageDialog.showError(e);
                }

            });
        });

        entryNewButton.setOnAction(evt ->
        {
            TreeItem<ConfigTreeItem> treeItem = tree.getSelectionModel().getSelectedItem();
            if (treeItem == null || treeItem.getValue() == null) {
                return;
            }
            final ConfigTreeItem configTreeItem = treeItem.getValue();

            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("new entry");
            dialog.setHeaderText("enter new entry name");
            dialog.showAndWait().ifPresent(newEntry ->
                    configTreeItem.getConfigNode().put(newEntry, ""));
            populateEntries(configTreeItem);
        });

        entryCopyButton.setOnAction(evt ->
        {
            ConfigEntry configEntry = entries.getSelectionModel().getSelectedItem();
            if (configEntry == null) {
                return;
            }

            TextInputDialog dialog = new TextInputDialog(configEntry.getKey());
            dialog.setTitle("copy entry");
            dialog.setHeaderText("enter new entry name");
            dialog.showAndWait().ifPresent(newEntry ->
                    configEntry.getConfigTreeItem().getConfigNode().put(newEntry, configEntry.getValue()));
            populateEntries(configEntry.getConfigTreeItem());
        });

        entryDeleteButton.setOnAction(evt ->
        {
            ConfigEntry configEntry = entries.getSelectionModel().getSelectedItem();
            if (configEntry == null) {
                return;
            }

            configEntry.getConfigTreeItem().getConfigNode().remove(configEntry.getKey());
            populateEntries(configEntry.getConfigTreeItem());
        });

        populateTree();
    }

    protected String findUniqueNodeName(Preferences node, String newName) throws BackingStoreException {
        int copy = 1;

        while (node.nodeExists(newName + "(" + copy + ")")) {
            copy++;
        }

        return newName + "(" + copy + ")";
    }

    protected Preferences copyNode(Preferences src, Preferences newNode) throws BackingStoreException {
        if (src.childrenNames().length > 0) {
            for (String child : src.childrenNames()) {
                copyNode(src.node(child), newNode.node(child));
            }
        }

        String[] keys = src.keys();
        for (String key : keys) {
            String value = src.get(key, "");
            newNode.put(key, value);
        }

        return newNode;
    }

    protected void populateEntries(ConfigTreeItem configTreeItem) {
        Preferences preferences = configTreeItem.getConfigNode();
        try {
            List<ConfigEntry> configEntries = new ArrayList<>();
            for (String key : preferences.keys()) {
                configEntries.add(new ConfigEntry(configTreeItem, key, preferences.get(key, null)));
            }

            entries.getItems().setAll(configEntries);
        } catch (BackingStoreException e) {
            entries.getItems().clear();
            MessageDialog.showError(e);
        }
    }

    public void initView() {
        populateTree();
        entries.getItems().clear();
    }

    protected void populateTree() {
        Preferences configNode = EnvironmentAccess.get();

        TreeItem<ConfigTreeItem> treeNode = new TreeItem<>(new ConfigTreeItem(configNode, "root"));
        treeNode.setExpanded(true);
        try {
            populateTree(treeNode, configNode);
        } catch (BackingStoreException e) {
            e.printStackTrace();
        }

        tree.setRoot(treeNode);
    }

    protected void importRegistry(File file) {
        InputStream is = null;
        try {
            is = new FileInputStream(file);

            Preferences.importPreferences(is);

            EnvironmentAccess.get().flush();
        } catch (Throwable e) {
            MessageDialog.showError(e);
            return;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    MessageDialog.showError(e);
                }
            }
        }

        MessageDialog.showMessage("Import erfolgreich!");

        initView();
    }

    protected void populateTreeNode(TreeItem<ConfigTreeItem> parentTreeNode, Preferences configNode) throws BackingStoreException {
        TreeItem<ConfigTreeItem> treeNode = new TreeItem<>(new ConfigTreeItem(configNode, configNode.name()));
        treeNode.setExpanded(true);

        parentTreeNode.getChildren().add(0, treeNode);

        populateTree(treeNode, configNode);
    }

    protected void populateTree(TreeItem<ConfigTreeItem> treeNode, Preferences configNode) throws BackingStoreException {
        for (String childName : configNode.childrenNames()) {
            Preferences childNode = configNode.node(childName);

            ConfigTreeItem configTreeItem = new ConfigTreeItem(childNode, childName);

            TreeItem<ConfigTreeItem> childTreeNode = new TreeItem<>(configTreeItem);
            treeNode.getChildren().add(childTreeNode);

            if (configNode.nodeExists(childName)) {
                populateTree(childTreeNode, childNode);
            }
        }
    }
}
