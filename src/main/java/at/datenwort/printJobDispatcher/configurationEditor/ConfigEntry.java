/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.configurationEditor;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by im on 23.03.17.
 */
public class ConfigEntry {
    private final ConfigTreeItem configTreeItem;
    private final SimpleStringProperty key = new SimpleStringProperty();
    private final SimpleStringProperty value = new SimpleStringProperty();

    public ConfigEntry(ConfigTreeItem configTreeItem, String key, String value) {
        this.configTreeItem = configTreeItem;

        this.key.set(key);
        this.key.addListener((observable, oldValue, newValue) ->
        {
            if (newValue == null || newValue.trim().isEmpty()) {
                this.key.set(oldValue);
                return;
            }

            configTreeItem.getConfigNode().remove(oldValue);
            configTreeItem.getConfigNode().put(newValue.trim(), getValue());
        });

        this.value.set(value);
        this.value.addListener((observable, oldValue, newValue) -> configTreeItem.getConfigNode().put(getKey(), newValue));
    }

    public ConfigTreeItem getConfigTreeItem() {
        return configTreeItem;
    }

    public String getKey() {
        return key.get();
    }

    public SimpleStringProperty keyProperty() {
        return key;
    }

    public String getValue() {
        return value.get();
    }

    public SimpleStringProperty valueProperty() {
        return value;
    }
}
