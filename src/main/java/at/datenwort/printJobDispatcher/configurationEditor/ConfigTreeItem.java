/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.configurationEditor;

import java.util.prefs.Preferences;

/**
 * Created by im on 23.03.17.
 */
public class ConfigTreeItem {
    private final Preferences configNode;
    private final String childName;

    public ConfigTreeItem(Preferences configNode, String childName) {
        this.configNode = configNode;
        this.childName = childName;
    }

    public Preferences getConfigNode() {
        return configNode;
    }

    public String getChildName() {
        return childName;
    }

    @Override
    public String toString() {
        return childName;
    }
}
