/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.config;

import at.datenwort.printJobDispatcher.lib.DocumentActionInterface;
import at.datenwort.printJobDispatcher.lib.collections.IdList;

import java.util.prefs.Preferences;

public class DocumentAction implements IdList.IdListEntry {
    private transient DocumentActionInterface documentAction;

    private transient String id;

    private String className;

    public DocumentAction() {
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void initializeDocumentAction(final Preferences prefs) {
        //TODO: this code is not yet solid enough, think about something different

        if (this.documentAction == null) {
            try {
                final DocumentActionInterface documentActionPlugin = (DocumentActionInterface) Class.forName(className).newInstance();
                documentActionPlugin.initializePlugin(prefs);
                this.documentAction = documentActionPlugin;
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public DocumentActionInterface getDocumentAction() {
        return documentAction;
    }
}
