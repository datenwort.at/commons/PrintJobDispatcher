/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.config;

import at.datenwort.printJobDispatcher.lib.EnvironmentAccess;
import at.datenwort.printJobDispatcher.lib.PreferencesUtils;
import at.datenwort.printJobDispatcher.lib.Utils;
import at.datenwort.printJobDispatcher.lib.collections.IdList;

import java.util.Arrays;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class PjdConfig {
    private static PjdConfig config;

    private String pdfprint;
    private String pdfprintcmd;

    private String spoolPath;
    private String networkBind;
    private String eloTemplatesPath;
    private String dmOutputPath;
    private String[] dmDeliveries;

    private Links links;
    private final IdList<DocumentAction> documentActions = new IdList<>();
    private final IdList<DocumentPatterns> documentPatterns = new IdList<>();
    private final IdList<OverlayConfig> overlayConfig = new IdList<>();
    private PrintJobsets printJobsets;
    private String spoolPathReplacement;

    public PjdConfig() {
    }

    public synchronized static PjdConfig getInstance() {
        if (config != null) {
            return config;
        }

        PjdConfig ret = new PjdConfig();

        Preferences root = EnvironmentAccess.get();
        try {
            root.sync(); // ensure we get the latest from the registry

            Preferences printJobDispatcher = root.node("print_job_dispatcher");
            if (printJobDispatcher == null) {
                return null;
            }
            printJobDispatcher.put("installed", "true");
            printJobDispatcher.flush();

            popuplateBaseSetup(printJobDispatcher, ret);

            Preferences documentActions = nodeIfExists(printJobDispatcher, "document_actions");
            if (documentActions != null) {
                populateDocumentActions(documentActions, ret);
            }

            Preferences documentPatterns = nodeIfExists(printJobDispatcher, "document_patterns");
            if (documentPatterns != null) {
                populateDocumentPatterns(documentPatterns, ret);
            }

            Preferences overlayConfigs = nodeIfExists(printJobDispatcher, "overlay_configs");
            if (overlayConfigs != null) {
                populateOverlayConfigs(overlayConfigs, ret);
            }

            Preferences printJobsets = nodeIfExists(printJobDispatcher, "print_jobsets");
            if (printJobsets != null) {
                populatePrintJobsets(printJobsets, ret);
            }

            Preferences links = nodeIfExists(printJobDispatcher, "links");
            if (links != null) {
                populateLinks(links, ret);
            }
        } catch (BackingStoreException e) {
            throw new RuntimeException(e);
        }

        config = ret;
        return ret;
    }

    private static void popuplateBaseSetup(Preferences printJobDispatcherNode, PjdConfig ret) {
        ret.setPdfprint(printJobDispatcherNode.get("pdfPrint", "pdfbox").toLowerCase());
        ret.setPdfprintcmd(printJobDispatcherNode.get("pdfPrintCmd", null));

        ret.setSpoolPath(printJobDispatcherNode.get("spoolPath", null));
        ret.setNetworkBind(printJobDispatcherNode.get("networkBind", null));

        // Preferences dm = printJobDispatcherNode.node("document_management");

        ret.setEloTemplatesPath(printJobDispatcherNode.get("eloTemplatesPath", null));
        ret.setDmOutputPath(printJobDispatcherNode.get("dmOutputPath", null));

        String dmDeliveries = printJobDispatcherNode.get("dmDeliveries", null);
        if (dmDeliveries != null) {
            ret.setDmDeliveries(dmDeliveries.split(" *[,; ] *"));
        }

        ret.setSpoolPathReplacement(printJobDispatcherNode.get("spoolPathReplacement", null));
    }

    private static void populateLinks(Preferences linksNode, PjdConfig ret) throws BackingStoreException {
        Links links = new Links();

        for (String linkId : sortedChildren(linksNode)) {
            Preferences linkNode = linksNode.node(linkId);

            Link link = new Link();
            link.setAbteilung(PreferencesUtils.get(linkNode, "abteilung", null));
            link.setAction(PreferencesUtils.get(linkNode, "action", null));
            link.setConverter(PreferencesUtils.get(linkNode, "converter", null));
            link.setLayout(PreferencesUtils.get(linkNode, "layout", null));
            link.setMandant(PreferencesUtils.get(linkNode, "mandant", null));
            link.setOverlay(PreferencesUtils.get(linkNode, "overlay", null));
            link.setPattern(PreferencesUtils.get(linkNode, "pattern", null));
            link.setTarget(PreferencesUtils.get(linkNode, "target", null));
            link.setPrinter(PreferencesUtils.get(linkNode, "printer", null));

            links.addLink(link);
        }

        ret.links = links;
    }

    private static String[] sortedChildren(Preferences linksNode) throws BackingStoreException {
        String[] names = linksNode.childrenNames();
        Arrays.sort(names);

        return names;
    }

    private static void populatePrintJobsets(Preferences printJobsetsNode, PjdConfig ret) throws BackingStoreException {
        PrintJobsets printJobsets = new PrintJobsets();

        for (String printJobsetId : sortedChildren(printJobsetsNode)) {
            Preferences printJobsetNode = printJobsetsNode.node(printJobsetId);

            PrintJobset printJobset = new PrintJobset();
            printJobset.setAbteilung(PreferencesUtils.get(printJobsetNode, "abteilung", null));
            printJobset.setMandant(PreferencesUtils.get(printJobsetNode, "mandant", null));
            printJobset.setPattern(PreferencesUtils.get(printJobsetNode, "pattern", null));
            printJobset.setSet(PreferencesUtils.get(printJobsetNode, "set", null));

            Preferences printJobDispatchNodes = nodeIfExists(printJobsetNode, "dispatch");
            if (printJobDispatchNodes != null) {
                for (String printJobDispatchId : sortedChildren(printJobDispatchNodes)) {
                    Preferences printJobDispatchNode = printJobDispatchNodes.node(printJobDispatchId);

                    PrintJobDispatch printJobDispatch = new PrintJobDispatch();
                    printJobDispatch.setCopies(PreferencesUtils.get(printJobDispatchNode, "copies", null));
                    printJobDispatch.setOverlay(PreferencesUtils.get(printJobDispatchNode, "overlay", null));
                    printJobDispatch.setPrintAttachments(printJobDispatchNode.getBoolean("attachments", false));
                    printJobDispatch.setQueue(PreferencesUtils.get(printJobDispatchNode, "queue", null));
                    printJobDispatch.setTray(PreferencesUtils.get(printJobDispatchNode, "tray", null));
                    printJobDispatch.setMonochrome(PreferencesUtils.get(printJobDispatchNode, "monochrome", false));

                    printJobset.addJobDispatch(printJobDispatch);
                }
            }

            printJobsets.addPrintJobset(printJobset);
        }

        ret.printJobsets = printJobsets;
    }

    private static void populateOverlayConfigs(Preferences overlayConfigsNode, PjdConfig ret) throws BackingStoreException {
        for (String overlayConfigId : sortedChildren(overlayConfigsNode)) {
            Preferences overlayConfigNode = overlayConfigsNode.node(overlayConfigId);

            OverlayConfig overlayConfig = new OverlayConfig();
            overlayConfig.setId(overlayConfigId);

            PDFOverlayConfiguration layerConfig = new PDFOverlayConfiguration();
            layerConfig.setId("pdf");

            for (String overlayLayerId : sortedChildren(overlayConfigNode)) {
                Preferences overlayLayerNode = overlayConfigNode.node(overlayLayerId);

                PDFOverlayLayer layer = new PDFOverlayLayer();
                layer.setPageOrder(PreferencesUtils.get(overlayLayerNode, "pageOrder", null));
                layer.setPath(PreferencesUtils.get(overlayLayerNode, "path", null));
                layer.setPosX(overlayLayerNode.getFloat("posX", 0f));
                layer.setPosY(overlayLayerNode.getFloat("posY", 0f));
                layer.setScaleX(overlayLayerNode.getFloat("scaleX", 0f));
                layer.setScaleY(overlayLayerNode.getFloat("scaleY", 0f));
                layer.setStacked(PreferencesUtils.get(overlayLayerNode, "stacked", null));

                layerConfig.addLayer(layer);
            }

            overlayConfig.addOverlay(layerConfig);

            ret.addOverlayConfig(overlayConfig);
        }
    }

    private static void populateDocumentPatterns(Preferences documentPatternsNode, PjdConfig ret) throws BackingStoreException {
        for (String documentPatternId : sortedChildren(documentPatternsNode)) {
            Preferences documentPatternNode = documentPatternsNode.node(documentPatternId);

            DocumentPatterns documentPatterns = new DocumentPatterns();
            documentPatterns.setId(documentPatternId);

            documentPatterns.setDocTypeName(documentPatternNode.get("docTypeName", null));

            Preferences documentPatternsConfigNode = nodeIfExists(documentPatternNode, "patterns");
            if (documentPatternsConfigNode != null) {
                for (String documentPatternsConfigId : sortedChildren(documentPatternsConfigNode)) {
                    Preferences documentPatternConfigNode = documentPatternsConfigNode.node(documentPatternsConfigId);

                    DocumentPattern documentPattern = new DocumentPattern();
                    documentPattern.setDocType(documentPatternConfigNode.getInt("docType", -1));
                    documentPattern.setNotMatch(documentPatternConfigNode.getBoolean("notMatch", false));
                    documentPattern.setPath(PreferencesUtils.get(documentPatternConfigNode, "path", null));
                    documentPattern.setRefDocumentPatterns(documentPatternConfigNode.get("refDocumentPatterns", null));

                    Preferences documentPatternCategoriesNode = nodeIfExists(documentPatternsConfigNode, "categories");
                    if (documentPatternCategoriesNode != null) {
                        for (String documentPatternCategoryId : sortedChildren(documentPatternCategoriesNode)) {
                            Preferences documentPatternCategoryNode = documentPatternCategoriesNode.node(documentPatternCategoryId);

                            documentPattern.addCategory(
                                    PreferencesUtils.get(documentPatternCategoryNode, "category", null),
                                    PreferencesUtils.get(documentPatternCategoryNode, "categoryRegEx", null));
                        }
                    }

                    documentPatterns.addPattern(documentPattern);
                }
            }

            Preferences documentTokenPatternsNode = nodeIfExists(documentPatternNode, "tokenPatterns");
            if (documentTokenPatternsNode != null) {
                for (String documentTokenPatternId : sortedChildren(documentTokenPatternsNode)) {
                    Preferences documentTokenPatternNode = documentTokenPatternsNode.node(documentTokenPatternId);

                    DocumentTokenPattern documentTokenPattern = new DocumentTokenPattern();
                    documentTokenPattern.setName(PreferencesUtils.get(documentTokenPatternNode, "name", null));
                    documentTokenPattern.setPattern(PreferencesUtils.get(documentTokenPatternNode, "pattern", null));

                    if (!Utils.isEmpty(documentTokenPattern.getName()) && !Utils.isEmpty(documentTokenPattern.getPattern())) {
                        documentPatterns.addDocumentTokenPattern(documentTokenPattern);
                    }
                }
            }

            ret.addDocumentPatterns(documentPatterns);
        }
    }

    private static void populateDocumentActions(Preferences documentActionsNode, PjdConfig ret) throws BackingStoreException {
        for (String documentActionId : sortedChildren(documentActionsNode)) {
            Preferences documentActionNode = documentActionsNode.node(documentActionId);

            if (documentActionNode.getBoolean("disabled", false)) {
                continue;
            }

            DocumentAction documentAction = new DocumentAction();

            documentAction.setId(documentActionId);
            documentAction.setClassName(PreferencesUtils.get(documentActionNode, "className", null));
            // documentAction.setPluginMutex(documentActionNode.get("pluginMutex", null));
            // documentAction.setPluginDir(documentActionNode.get("pluginDir", null));

            Preferences configPrefs = nodeIfExists(documentActionNode, "config");
            documentAction.initializeDocumentAction(configPrefs);

            ret.addDocumentAction(documentAction);
        }
    }

    private static Preferences nodeIfExists(Preferences node, String nodeName) throws BackingStoreException {
        if (!node.nodeExists(nodeName)) {
            return null;
        }

        return node.node(nodeName);
    }

    public String getPdfprint() {
        return pdfprint;
    }

    public void setPdfprint(String pdfprint) {
        this.pdfprint = pdfprint;
    }

    public String getPdfprintcmd() {
        return pdfprintcmd;
    }

    public void setPdfprintcmd(String pdfprintcmd) {
        this.pdfprintcmd = pdfprintcmd;
    }

    public String getSpoolPath() {
        return spoolPath;
    }

    public void setSpoolPath(String spoolPath) {
        this.spoolPath = spoolPath;
    }

    public DocumentPatterns getDocumentPatterns(String name) {
        return documentPatterns.getById(name);
    }

    public IdList<DocumentPatterns> getDocumentPatterns() {
        return documentPatterns;
    }

    public void addDocumentPatterns(DocumentPatterns documentPatterns) {
        this.documentPatterns.add(documentPatterns);
    }

    public OverlayConfig getOverlayConfig(String name) {
        return overlayConfig.getById(name);
    }

    public void addOverlayConfig(OverlayConfig overlay) {
        overlayConfig.add(overlay);
    }

    public DocumentAction getDocumentActions(String name) {
        return documentActions.getById(name);
    }

    public void addDocumentAction(DocumentAction action) {
        documentActions.add(action);
    }

    public IdList<DocumentAction> getDocumentActions() {
        return documentActions;
    }

    public IdList<OverlayConfig> getOverlayConfig() {
        return overlayConfig;
    }

    public Links getLinks() {
        return links;
    }

    public PrintJobsets getPrintJobsets() {
        return printJobsets;
    }

    public String getNetworkBind() {
        return networkBind;
    }

    public void setNetworkBind(String networkBind) {
        this.networkBind = networkBind;
    }

    public String getEloTemplatesPath() {
        return eloTemplatesPath;
    }

    public void setEloTemplatesPath(String eloTemplatesPath) {
        this.eloTemplatesPath = eloTemplatesPath;
    }

    public String getDmOutputPath() {
        return dmOutputPath;
    }

    public void setDmOutputPath(String dmOutputPath) {
        this.dmOutputPath = dmOutputPath;
    }

    public String[] getDmDeliveries() {
        return dmDeliveries;
    }

    public void setDmDeliveries(String[] dmDeliveries) {
        this.dmDeliveries = dmDeliveries;
    }

    public String getSpoolPathReplacement() {
        return spoolPathReplacement;
    }

    public void setSpoolPathReplacement(String spoolPathReplacement) {
        this.spoolPathReplacement = spoolPathReplacement;
    }
}
