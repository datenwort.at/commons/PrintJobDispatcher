/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.config;

import at.datenwort.printJobDispatcher.lib.Utils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Link extends at.datenwort.printJobDispatcher.lib.config.system.Link {
    private String target;
    private String pattern;
    private String converter;
    private String overlay;
    private String action;
    private String abteilung;
    private String layout;
    private String printer;

    private final Set<String> targets = new HashSet<>();

    public Link() {
    }

    @Override
    public String getDataSummary() {
        StringBuilder sb = new StringBuilder(super.getDataSummary());
        if (getTarget() != null) {
            sb.append(" ");
            sb.append(getTarget());
        }
        if (getPattern() != null) {
            sb.append(" ");
            sb.append(getPattern());
        }
        if (getConverter() != null) {
            sb.append(" ");
            sb.append(getConverter());
        }
        if (getOverlay() != null) {
            sb.append(" ");
            sb.append(getOverlay());
        }
        if (getAction() != null) {
            sb.append(" ");
            sb.append(getAction());
        }
        if (getAbteilung() != null) {
            sb.append(" ");
            sb.append(getAbteilung());
        }
        if (getLayout() != null) {
            sb.append(" ");
            sb.append(getLayout());
        }
        if (getPrinter() != null) {
            sb.append(" ");
            sb.append(getPrinter());
        }
        return sb.toString();
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        if (Utils.isEmpty(target)) {
            return;
        }

        this.target = target;

        if (target != null) {
            targets.addAll(Arrays.asList(target.split(", *")));
        } else {
            targets.clear();
        }
    }

    public boolean hasTargets() {
        return targets != null && targets.size() > 0;
    }

    public boolean hasTarget(String target) {
        if (targets == null) {
            return false;
        }

        return targets.contains(target);
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getPrinter() {
        return printer;
    }

    public void setPrinter(String printer) {
        this.printer = printer;
    }

    public String getConverter() {
        return converter;
    }

    public void setConverter(String converter) {
        this.converter = converter;
    }

    public String getOverlay() {
        return overlay;
    }

    public void setOverlay(String overlay) {
        this.overlay = overlay;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAbteilung() {
        return abteilung;
    }

    public void setAbteilung(String abteilung) {
        this.abteilung = abteilung;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }
}
