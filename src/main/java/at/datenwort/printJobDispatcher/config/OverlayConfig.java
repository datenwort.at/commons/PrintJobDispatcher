/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.config;

import at.datenwort.printJobDispatcher.lib.collections.IdList;

public class OverlayConfig implements IdList.IdListEntry {
    private String id;

    private final IdList<PDFOverlayConfiguration> overlays = new IdList<>();

    public OverlayConfig() {
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void addOverlay(PDFOverlayConfiguration overlay) {
        overlays.add(overlay);
    }

    public IdList<PDFOverlayConfiguration> getOverlays() {
        return overlays;
    }

    public PDFOverlayConfiguration getOverlay(String name) {
        return overlays.getById(name);
    }

}
