/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.config;

import at.datenwort.printJobDispatcher.dblib.DocumentInterface;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class DocumentPattern {
    private int docType;

    private final List<CategoryEntry> categories = new ArrayList<>();

    private boolean notMatch;
    private String refDocumentPatterns;

    private String path;
    private transient Pattern pathRegEx;

    private String contentPattern;
    private transient Pattern contentPatternRegEx;

    public DocumentPattern() {
        docType = -1;
        notMatch = false;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public Pattern getPathRegEx() {
        if (path != null) {
            pathRegEx = Pattern.compile(path);
        } else {
            pathRegEx = null;
        }

        return pathRegEx;
    }

    public String getContentPattern() {
        return contentPattern;
    }

    public void setContentPattern(String contentPattern) {
        if (contentPattern != null) {
            contentPatternRegEx = Pattern.compile(contentPattern, Pattern.MULTILINE);
        } else {
            contentPatternRegEx = null;
        }
        this.contentPattern = contentPattern;
    }

    public void addCategory(String category, String categoryRegEx) {
        addCategory(category, Pattern.compile(categoryRegEx));
    }

    public void addCategory(String category, Pattern categoryRegEx) {
        addCategory(new CategoryEntry(category, categoryRegEx));
    }

    public void setNotMatch(boolean notMatch) {
        this.notMatch = notMatch;
    }

    public boolean isNotMatch() {
        return notMatch;
    }

    private void addCategory(CategoryEntry entry) {
        categories.add(entry);
    }

    public void setDocType(int docType) {
        this.docType = docType;
    }

    public int getDocType() {
        return docType;
    }

    public List<CategoryEntry> getCategories() {
        return categories;
    }

    public String getRefDocumentPatterns() {
        return refDocumentPatterns;
    }

    public void setRefDocumentPatterns(String refDocumentPatterns) {
        this.refDocumentPatterns = refDocumentPatterns;
    }

    public boolean matches(DocumentInterface doc) {
        return matches(doc, isNotMatch());
    }

    private boolean matches(DocumentInterface doc, boolean notMatch) {
        if (refDocumentPatterns != null) {
            DocumentPatterns refDocPatterns = PjdConfig.getInstance().getDocumentPatterns(refDocumentPatterns);
            if (refDocPatterns == null) {
                throw new IllegalArgumentException("RefDocumentPatterns '" + refDocumentPatterns + "' not found.");
            }

            Iterator<DocumentPattern> iterDocPatterns = refDocPatterns.getPatterns().iterator();
            while (iterDocPatterns.hasNext()) {
                DocumentPattern pattern = iterDocPatterns.next();
                if (!pattern.matches(doc, notMatch)) {
                    return false;
                }
            }
        }

        if (contentPatternRegEx != null) {
            String documentContent = doc.getContentAsText();
            if (documentContent == null || !contentPatternRegEx.matcher(documentContent).find()) {
                return false;
            }
        }

        if (docType > -1) {
            if (doc.getDoctype() != docType) {
                if (!notMatch) {
                    return false;
                }
            }
        }

        if (getPathRegEx() != null) {
            if (doc.getPath() == null || !getPathRegEx().matcher(doc.getPath().getAbsolutePath()).matches()) {
                if (!notMatch) {
                    return false;
                }
            }
        }

        if (categories != null) {
            // DocCats dcs = doc.getCategories(new DocCatKey());

            Iterator<CategoryEntry> iterCategories = categories.iterator();
            nextEntry:
            while (iterCategories.hasNext()) {
                CategoryEntry entry = iterCategories.next();
                String category = entry.getCategory();

                String[] docCatValues = doc.getCategoryValues(category);
                // List docCatValues = dcs.getValues(doc.getNr(), category);
                if (docCatValues == null) {
                    if (!notMatch) {
                        return false;
                    }
                    continue;
                }

                // Iterator iterDocCatValues = docCatValues.iterator();
                // while (iterDocCatValues.hasNext())
                for (String docCatValue : docCatValues) {
                    // String docCatValue = (String) iterDocCatValues.next();
                    if (entry.getCategoryRegEx().matcher(docCatValue).matches()) {
                        if (!notMatch) {
                            continue nextEntry;
                        } else {
                            return false;
                        }
                    }
                }

                if (!notMatch) {
                    return false;
                }
            }
        }

        return true;
    }
}
