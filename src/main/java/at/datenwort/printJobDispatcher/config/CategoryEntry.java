/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.config;

import java.util.regex.Pattern;

public class CategoryEntry {
    private String category;
    private Pattern categoryRegEx;

    public CategoryEntry(final String category, final Pattern categoryRegEx) {
        this.category = category;
        this.categoryRegEx = categoryRegEx;
    }

    public CategoryEntry() {
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCategoryRegEx(Pattern categoryRegEx) {
        this.categoryRegEx = categoryRegEx;
    }

    public String getCategory() {
        return category;
    }

    public Pattern getCategoryRegEx() {
        return categoryRegEx;
    }
}
