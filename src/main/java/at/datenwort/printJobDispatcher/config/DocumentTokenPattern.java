/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.config;

import at.datenwort.printJobDispatcher.dblib.DocumentInterface;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DocumentTokenPattern {
    private String name;
    private String pattern;

    private transient Pattern tokenPatternRegEx;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        if (pattern != null) {
            tokenPatternRegEx = Pattern.compile(pattern, Pattern.MULTILINE);
        } else {
            tokenPatternRegEx = null;
        }
        this.pattern = pattern;
    }

    public void processDocument(DocumentInterface doc) {
        if (tokenPatternRegEx == null) {
            return;
        }

        String content = doc.getContentAsText();
        if (content == null) {
            return;
        }

        Matcher matcher = tokenPatternRegEx.matcher(content);
        while (matcher.find()) {
            String value = matcher.group("value");

            doc.addCategoryValue(name, value);
        }
    }
}
