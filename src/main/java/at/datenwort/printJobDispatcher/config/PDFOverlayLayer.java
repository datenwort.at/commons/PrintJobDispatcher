/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.config;

public class PDFOverlayLayer {
    public final static String STACK_UNDER = "U";
    public final static String STACK_OVER = "O";

    private String path;
    private String stacked;
    private String pageOrder;
    private float scaleX;
    private float scaleY;
    private float posX;
    private float posY;

    public PDFOverlayLayer() {
        scaleX = 1f;
        scaleY = 1f;
        posX = 0;
        posY = 0;
        stacked = STACK_OVER;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public float getScaleX() {
        return scaleX;
    }

    public String getPageOrder() {
        return pageOrder;
    }

    /**
     * <ul>
     * <li>default<br>
     * page 1
     * <li>mod<br>
     * modulo
     * <li>1,2<br>
     * page 1, then always page 2
     * </ul>
     */
    public void setPageOrder(String pageOrder) {
        this.pageOrder = pageOrder;
    }

    public void setScaleX(float scaleX) {
        this.scaleX = scaleX;
    }

    public float getScaleY() {
        return scaleY;
    }

    public void setScaleY(float scaleY) {
        this.scaleY = scaleY;
    }

    public float getPosX() {
        return posX;
    }

    public void setPosX(float posX) {
        this.posX = posX;
    }

    public float getPosY() {
        return posY;
    }

    public void setPosY(float posY) {
        this.posY = posY;
    }

    public String getStacked() {
        return stacked;
    }

    public void setStacked(String stacked) {
        if (stacked != null) {
            if (stacked.startsWith("O")) {
                this.stacked = STACK_OVER;
                return;
            } else if (stacked.startsWith("U")) {
                this.stacked = STACK_UNDER;
                return;
            }
        }

        throw new IllegalArgumentException("unknown stacked: " + stacked);
    }

}
