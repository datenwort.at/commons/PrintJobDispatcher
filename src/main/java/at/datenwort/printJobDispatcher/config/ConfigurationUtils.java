/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.config;

import at.datenwort.printJobDispatcher.dblib.DocumentInterface;
import at.datenwort.printJobDispatcher.lib.Utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ConfigurationUtils {
    private ConfigurationUtils() {
    }

    public static PDFOverlayConfiguration getOverlayConfig(String mandant, String target, String abteilung, String layout, PjdConfig configuration, Set<String> patterns, String name, String printer) {
        if (configuration == null) {
            return null;
        }

        Links links = configuration.getLinks();
        if (links == null) {
            return null;
        }

        Iterator<Link> iterLinks = links.iterator();
        while (iterLinks.hasNext()) {
            Link link = iterLinks.next();
            if (link.getPattern() == null) {
                // required field not setup
                continue;
            }

            if (!Utils.isEmpty(link.getMandant()) && !link.getMandant().equals(mandant)) {
                continue;
            }
            if (link.hasTargets() && target != null && !link.hasTarget(target)) {
                continue;
            }

            if (link.getAbteilung() != null && !link.getAbteilung().equals(abteilung)) {
                continue;
            }

            if (link.getLayout() != null && !link.getLayout().equals(layout)) {
                continue;
            }

            if (link.getPrinter() != null && !link.getPrinter().equals(printer)) {
                continue;
            }

            if (!patterns.contains(link.getPattern()) || link.getOverlay() == null) {
                continue;
            }

            OverlayConfig overlays = configuration.getOverlayConfig(link.getOverlay());
            if (overlays == null) {
                continue;
            }

            PDFOverlayConfiguration overlay = overlays.getOverlay(name);
            if (overlay != null) {
                return overlay;
            }
        }

        return null;
    }

    public static PrintJobset getPrintJobset(String mandant, PjdConfig configuration, String set, String abteilung, Set patterns) {
        if (configuration == null) {
            return null;
        }

        PrintJobsets printJobsets = configuration.getPrintJobsets();
        if (printJobsets == null) {
            return null;
        }

        Iterator<PrintJobset> iterPrintJobsets = printJobsets.iterator();
        while (iterPrintJobsets.hasNext()) {
            PrintJobset jobset = iterPrintJobsets.next();
            if (jobset.getSet() != null && Utils.compare(jobset.getSet(), set) != 0) {
                continue;
            }

            if (!Utils.isEmpty(jobset.getMandant()) && !jobset.getMandant().equals(mandant)) {
                continue;
            }

            if (jobset.getAbteilung() != null && !jobset.getAbteilung().equals(abteilung)) {
                continue;
            }

            if (jobset.getPattern() != null && (patterns == null || !patterns.contains(jobset.getPattern()))) {
                continue;
            }

            return jobset;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static Set<String> findPatternIds(PjdConfig configuration, DocumentInterface doc) {
        if (configuration == null) {
            return Collections.EMPTY_SET;
        }

        Set<String> patternsSet = new TreeSet<>();

        Collection<DocumentPatterns> patterns = configuration.getDocumentPatterns();
        for (DocumentPatterns docPatterns : patterns) {
            // TODO: Matched zeugs dürfte falsch sein
            // boolean matched = false;

            for (DocumentPattern documentPattern : docPatterns.getPatterns()) {
                if (documentPattern.matches(doc)) {
                    // matched = true;
                    patternsSet.add(docPatterns.getId());
                }
            }

            /*
            if (matched)
            {
                docPatterns.processDoc(doc);
            }
            */
        }

        return patternsSet;
    }

    public static List<DocumentAction> getDocumentActions(String mandant, PjdConfig configuration, Set patterns, String printer) {
        if (configuration == null) {
            return null;
        }

        Links links = configuration.getLinks();
        if (links == null) {
            return null;
        }

        Iterator<Link> iterLinks = links.iterator();
        while (iterLinks.hasNext()) {
            Link link = iterLinks.next();
            if (!Utils.isEmpty(link.getMandant()) && !link.getMandant().equals(mandant)) {
                continue;
            }

            if (link.getAction() == null) {
                continue;
            }

            if (link.getPrinter() != null && !link.getPrinter().equals(printer)) {
                continue;
            }

            if (link.getPattern() != null && !patterns.contains(link.getPattern())) {
                continue;
            }

            List<DocumentAction> ret = new ArrayList<>();

            String[] actions = Utils.splitString(link.getAction());
            for (String actionId : actions) {
                DocumentAction action = configuration.getDocumentActions(actionId);
                if (action == null) {
                    continue;
                }

                ret.add(action);
            }

            return ret;
        }

        return null;
    }
}
