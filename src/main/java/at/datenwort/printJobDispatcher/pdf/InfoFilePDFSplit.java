/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.pdf;

import at.datenwort.printJobDispatcher.dblib.DocumentInterface;
import at.datenwort.printJobDispatcher.dm.DocumentManagementDelivery;
import at.datenwort.printJobDispatcher.lib.RegexSubstitutor;
import at.datenwort.printJobDispatcher.lib.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.io.RandomAccessReadBufferedFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InfoFilePDFSplit /* implements ApplicationThreadControllable */ {
    private final static Log log = LogFactory.getLog(InfoFilePDFSplit.class);
    private final static Pattern TOKEN_PATTERN = Pattern.compile("\\$[a-z0-9]+=[^(\\$|\r|\n)]+(\\$|\r|\n)", Pattern.CASE_INSENSITIVE);

    public final static String TOKEN_PAGE = "Seite=";

    private File pdfindir = null;
    private File pdfoutdir = null;
    private Pattern regularExp = null;
    private List<DocumentInterface> collect = null;

    private long m_nDelay = -1;
    private boolean m_bInterrupt = false;

    private DocumentManagementDelivery documentManagementDelivery;

    private static class VirtualDocument implements DocumentInterface {
        private final File file;
        private final String simpleName;
        private final String extDocumentNumber;
        private final DocumentTokens info;

        private VirtualDocument(final File file, String simpleName, String extDocumentNumber, final DocumentTokens info) {
            this.file = file;
            this.simpleName = simpleName;
            this.extDocumentNumber = extDocumentNumber;
            this.info = info;
        }

        public String getMandant() {
            return info.getMandant();
        }

        public int getDoctype() {
            return 0;
        }

        public File getPath() {
            return file;
        }

        public String getSimpleName() {
            return simpleName;
        }

        public String getExtDocumentNumber() {
            return extDocumentNumber;
        }

        public void addCategoryValue(String name, String value) {
            info.addToken(name, value);
        }

        public String[] getCategoryValues(String cat) {
            Set<String> tokenValues = info.getTokenValues(cat);
            if (tokenValues == null) {
                return null;
            }

            String[] ret = new String[tokenValues.size()];
            tokenValues.toArray(ret);
            return ret;
        }

        public String getCategoryValue(String cat) {
            Set<String> tokenValues = info.getTokenValues(cat);
            if (tokenValues == null || tokenValues.size() < 1) {
                return null;
            }

            return tokenValues.iterator().next();
        }

        public String getCategoryValueAll(String cat) {
            Set<String> tokenValues = info.getTokenValues(cat);
            if (tokenValues == null || tokenValues.size() < 1) {
                return null;
            }

            return Utils.join(",", tokenValues);
        }

        public InputStream getData(int asType) {
            return null;
        }

        public String getUsername() {
            return info.getUser();
            // return Utils.getFirstOrNull(getCategoryValues(Cazador.CATN_USER));
        }

        public DocumentTokens getInfo() {
            return info;
        }

        @Override
        public String getContentAsText() {
            return info.getContent();
        }
    }

    private class SplitEventListener implements PDFSplit.EventInterface {
        private final Map<Integer, PageTokens> pageInfo;
        private DocumentTokens info = null;

        private SplitEventListener(Map<Integer, PageTokens> pageInfo) {
            this.pageInfo = pageInfo;
        }

        public void startDocument() {
            info = new DocumentTokens();
        }

        public void addPage(int pageNum) {
            PageTokens pageInfoNum = pageInfo.get(pageNum);
            if (pageInfoNum == null) {
                return;
            }

            info.addContent(pageInfoNum.getContent());

            Set tokens = pageInfoNum.getTokens();
            Iterator iterTokens = tokens.iterator();
            while (iterTokens.hasNext()) {
                String token = (String) iterTokens.next();
                if (!token.startsWith(TOKEN_PAGE)) {
                    info.addToken(token);
                }
            }
        }

        public void documentCreated(String filename) {
            try {
                renameDocument(filename, info);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private class BuildPageInfo implements PDFTextStripper.EventInterface {
        private final StringWriter buf;
        private final Map<Integer, PageTokens> pageInfo;

        private BuildPageInfo(Map<Integer, PageTokens> pageInfo, StringWriter buf) {
            this.buf = buf;
            this.pageInfo = pageInfo;
        }

        public void pageProcessed(int pagenum) {
            String docText = this.buf.toString();
            this.buf.getBuffer().setLength(0);

            Matcher matcher = TOKEN_PATTERN.matcher(docText);

            Integer key = pagenum;
            PageTokens currentPageInfo = pageInfo.get(key);
            if (currentPageInfo == null) {
                currentPageInfo = new PageTokens(pagenum);
                pageInfo.put(key, currentPageInfo);
            }
            currentPageInfo.addContent(docText);

            while (matcher.find()) {
                String token = docText.substring(matcher.start() + 1, matcher.end() - 1);
                if (log.isErrorEnabled()) {
                    log.debug("page: " + pagenum + " token: " + token);
                }

                currentPageInfo.addToken(token);
            }
        }
    }

    public InfoFilePDFSplit() {
    }

    public void setCollectInto(List<DocumentInterface> collect) {
        this.collect = collect;
    }

    public void splitDocument(File fileIn, String fileOut) throws Exception {
        PDDocument doc = null;

        // EXTRACT TEXT (and Tokens ($abc=def$)
        PDFTextStripper stripper = new PDFTextStripper();
        stripper.setSortByPosition(true);

        PDFParser parser;
        try (RandomAccessReadBufferedFile input = new RandomAccessReadBufferedFile(fileIn)) {
            parser = new PDFParser(input);
        }

        try {
            doc = parser.parse();
            StringWriter sw = new StringWriter(1024000);
            Map<Integer, PageTokens> pageInfo = new TreeMap<>();
            stripper.setEventListener(new BuildPageInfo(pageInfo, sw));
            stripper.writeText(doc, sw);
            sw.close();


            // Split every invoice into its own pdf
            PDFSplit split = new PDFSplit();

            // split.setDocument(doc);
            try (FileInputStream input = new FileInputStream(fileIn)) {
                split.setDocumentStream(input);
                split.setOutputFileName(fileOut);
                split.setEventListener(new SplitEventListener(pageInfo));
                split.setSplitAction(new FirstPageSplit(pageInfo));

                split.process();
            }
        } finally {
            if (doc != null) {
                doc.close();
            }
        }
    }

    private void renameDocument(String filename, DocumentTokens info) throws ParserConfigurationException, TransformerException, IOException, BackingStoreException {
        String sMandant = info.getMandant();
        if (sMandant == null) {
            sMandant = "XX";
            // throw new IllegalArgumentException("Mandant in PDF nicht gefunden.");
        }

        // create a new, more descriptive filename
        String aco = info.getACO();
        if (aco == null) {
            aco = "Liste";
        }

        long uniquePrintCounter = getNextListNumber();

        String nr = info.getTokenValue("Belegnummer");
        if (nr == null) {
            nr = info.getTokenValue("Auftragsnummer");
        }


        String simpleName = aco + (nr != null ? nr : Long.toString(uniquePrintCounter));

        File newFile;
        int count = 0;
        do {
            String newFilename = aco + (nr != null ? nr : "") + "_" + uniquePrintCounter + "_" + count + ".pdf";
            newFilename = newFilename.replaceAll("[^a-zA-Z0-9_\\.]", "_");

            newFile = new File(pdfoutdir, newFilename);
            count++;
        }
        while (newFile.exists());
        File pdfFile = new File(filename);

        if (!pdfFile.renameTo(newFile)) {
            throw new IOException("Rename " + pdfFile.toString() + " to " + newFile.getAbsolutePath() + " failed.");
        }
        pdfFile = newFile;

        String exNr = nr;
        if (exNr == null) {
            exNr = simpleName;
        }
        DocumentInterface doc = new VirtualDocument(pdfFile, simpleName, exNr, info);
        if (collect != null) {
            collect.add(doc);
        }

        DocumentManagementDelivery dmd = getDocumentManagementDelivery();
        if (dmd != null) {
            dmd.deliverDocument(pdfFile, doc);
        }

        /*
        // build cazador file
        buildCazadorFile(pdfFile, sMandant, info);
        */

        // create elo file
        // createEloFile(sMandant, pdfFile, info);
    }

    private long getNextListNumber() throws BackingStoreException {
        synchronized (InfoFilePDFSplit.class) {
            Preferences prefs = Preferences.systemNodeForPackage(InfoFilePDFSplit.class);
            long ret = prefs.getLong("NextListNumber", 0);
            if (ret > 9999999999L) {
                ret = 0;
            }
            prefs.putLong("NextListNumber", ret + 1);
            prefs.sync();
            return ret;
        }
    }

    public void process() throws IOException {
        while (!Thread.interrupted() && !m_bInterrupt) {
            if (!processFiles()) {
                return;
            }

            if (Thread.currentThread().isInterrupted() || m_bInterrupt) {
                continue;
            }

            Thread.yield();
            try {
                if (m_nDelay > 0) {
                    Thread.sleep(m_nDelay);
                } else {
                    Thread.sleep(10000);
                }
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    /**
     * @return false to stop any further processing. e.g. Thread.isInterrupted or Exception flood
     */
    public boolean processFiles() throws IOException {
        File pdfs[] = this.pdfindir.listFiles((dir, name) -> regularExp.matcher(name).find());

        if (pdfs == null) {
            return true;
        }

        int nuofException = 0;
        for (int i = 0; i < pdfs.length; i++) {
            if (Thread.currentThread().isInterrupted() || m_bInterrupt) {
                return false;
            }

            File pdfIn = pdfs[i];
            String pdfPath = pdfIn.toString();
            String pdfOut = RegexSubstitutor.replaceLastCount(
                    new File(pdfoutdir, pdfIn.getName()).toString(),
                    regularExp,
                    "_{0}$1",
                    1);

            long time = System.currentTimeMillis();
            try {
                log.info("CazadorPDFSplit start " + pdfIn.getAbsolutePath());

                splitDocument(new File(pdfPath), pdfOut);
                File pdfInDone = new File(pdfIn.toString() + ".done");
                if (!pdfIn.renameTo(pdfInDone)) {
                    throw new IOException("Rename " + pdfIn.toString() + " to " + pdfInDone.toString() + " failed.");
                }
                nuofException = 0;
            } catch (Exception e) {
                log.fatal(e.getLocalizedMessage(), e);
                nuofException++;

                // three excpetions in one turn, seems like a general fault.
                // exit to avoid exception flooding
                if (nuofException > 3) {
                    return false;
                }
            } finally {
                log.info("CazadorPDFSplit: " + pdfIn.getName() + " duration:" + (System.currentTimeMillis() - time) + "ms");
            }
        }

        return true;
    }

    public void setPDFInDir(String pdfindir) {
        this.pdfindir = new File(pdfindir);
    }

    public void setPDFOutDir(String pdfoutdir) {
        this.pdfoutdir = new File(pdfoutdir);
    }

    public void setPDFNameMask(String regularExp) {
        this.regularExp = Pattern.compile(regularExp);
    }


    public void setDocumentManagementDelivery(DocumentManagementDelivery documentManagementDelivery) {
        this.documentManagementDelivery = documentManagementDelivery;
    }

    public DocumentManagementDelivery getDocumentManagementDelivery() {
        return documentManagementDelivery;
    }
}
