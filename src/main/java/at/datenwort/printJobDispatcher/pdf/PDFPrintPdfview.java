/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.pdf;

import at.datenwort.printJobDispatcher.lib.Utils;
import at.datenwort.printJobDispatcher.lib.printer.PrinterDevice;
import at.datenwort.printJobDispatcher.lib.printer.PrinterEnvironment;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PDFRenderer;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import javax.print.attribute.Attribute;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Chromaticity;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.Media;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.PrinterName;
import java.awt.*;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class PDFPrintPdfview implements PDFPrint {
    public void print(String printer, File pdfFile, String title, Integer copies, String tray, boolean monochrome) throws IOException, InterruptedException, PrintException, PrinterException {
        AttributeSet aset = new HashPrintServiceAttributeSet();
        aset.add(new PrinterName(printer, null));
        PrintService[] pss = javax.print.PrintServiceLookup.lookupPrintServices(DocFlavor.SERVICE_FORMATTED.PAGEABLE, aset);

        if (pss == null || pss.length < 1) {
            throw new IllegalArgumentException("printer " + printer + " not found");
        }
        PrintService ps = pss[0];

        final PrinterDevice pd = PrinterEnvironment.getPrinterEnvironment().findDevice(printer);
        if (pd == null) {
            throw new IllegalArgumentException("printer " + printer + " not found");
        }

        // set up the PDF reading
        try (RandomAccessFile raf = new RandomAccessFile(pdfFile, "r")) {

            FileChannel channel = raf.getChannel();
            ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
            final PDFFile pdffile = new PDFFile(buf);

            Attribute mediaTray = null;
            if (tray != null) {
                tray = pd.getTray(tray);

                Attribute[] medias = (Attribute[]) ps.getSupportedAttributeValues(Media.class, null, null);
                for (Attribute media : medias) {
                    if (tray.equals(media.toString())) {
                        mediaTray = media;
                        break;
                    }
                }

                if (mediaTray == null) {
                    StringBuilder sb = new StringBuilder();
                    for (Attribute media : medias) {
                        if (sb.length() > 0) {
                            sb.append(",");
                        }
                        sb.append(media.toString());
                    }

                    throw new UnsupportedOperationException("tray '" + tray + "' not available on printer '" + ps.getName() + "'. Supported trays:" + sb);
                }
            }

            // derive orientation from first page
            PDFPage page = pdffile.getNumPages() > 0 ? pdffile.getPage(1) : null;
            DocAttributeSet daset = new HashDocAttributeSet();
            if (page != null && (page.getRotation() == 90 || page.getRotation() == 270)) {
                daset.add(OrientationRequested.LANDSCAPE);
            } else {
                daset.add(OrientationRequested.PORTRAIT);
            }

            DocFlavor flavor = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
            PrintRequestAttributeSet praset = new HashPrintRequestAttributeSet();
            if (mediaTray != null) {
                praset.add(mediaTray);
            } else {
                praset.add(MediaSizeName.ISO_A4);
            }
            if (copies != null) {
                praset.add(new Copies(copies));
            }
            if (!Utils.isEmpty(title)) {
                praset.add(new JobName(title, null));
            }
            if (monochrome) {
                praset.add(Chromaticity.MONOCHROME);
            }

            DocPrintJob printJob = ps.createPrintJob();

            Printable printable = (graphics, format, pageIndex) ->
            {
                if (pageIndex >= 0 && pageIndex < pdffile.getNumPages()) {
                    Graphics2D g2 = (Graphics2D) graphics;
                    PDFPage page1 = pdffile.getPage(pageIndex + 1);

                    // we assume that the print job is already within the right bounds
                    Rectangle imgbounds = new Rectangle(
                            pd.getRelativeMarginX(),
                            pd.getRelativeMarginY(),
                            (int) page1.getWidth() - pd.getRelativeMarginW(),
                            (int) page1.getHeight() - pd.getRelativeMarginH());

                    PDFRenderer pgs = new PDFRenderer(page1, g2, imgbounds, null, null);
                    try {
                        page1.waitForFinish();
                        pgs.run();
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }

                    return Printable.PAGE_EXISTS;
                } else {
                    return Printable.NO_SUCH_PAGE;
                }
            };
            Doc doc = new SimpleDoc(printable, flavor, daset);

            printJob.print(doc, praset);
        }
    }

    public static void main(String[] args) throws PrintException, IOException, PrinterException, InterruptedException {
        PDFPrint print = new PDFPrintPdfview();
        print.print("Kyocera_ECOSYS_M5526cdw",
                new File("/Users/im/Downloads/data.pdf"),
                "test",
                1,
                null,
                false);
    }
}
