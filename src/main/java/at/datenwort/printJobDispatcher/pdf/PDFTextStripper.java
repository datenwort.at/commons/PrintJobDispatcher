/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.pdf;


import org.apache.pdfbox.pdmodel.PDPage;

import java.io.IOException;


/**
 * This class will take a pdf document and strip out all of the text and ignore the
 * formatting and such
 *
 * @author Ben Litchfield (ben@csh.rit.edu)
 * @version $Revision: 12212 $
 */
public class PDFTextStripper extends org.apache.pdfbox.text.PDFTextStripper {
    private EventInterface eventInterface = null;

    private int pageNum = 0;

    public interface EventInterface {
        void pageProcessed(int pagenum);
    }

    public PDFTextStripper() throws IOException {
        super();
    }

    @Override
    public void processPage(PDPage page) throws IOException {
        pageNum++;

        super.processPage(page);

        if (eventInterface != null) {
            eventInterface.pageProcessed(pageNum);
        }
    }

    /**
     * Set your event listener
     *
     * @see PDFTextStripper.EventInterface
     */
    public void setEventListener(EventInterface eventListener) {
        this.eventInterface = eventListener;
    }
}
