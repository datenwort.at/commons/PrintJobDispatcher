/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.pdf;

import at.datenwort.printJobDispatcher.config.PDFOverlayConfiguration;
import at.datenwort.printJobDispatcher.config.PDFOverlayLayer;
import at.datenwort.printJobDispatcher.lib.Utils;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class PDFOverlayer {
    private final static Log log = LogFactory.getLog(PDFOverlayer.class);

    private final PDFOverlayConfiguration overlayConfig;
    private byte[] input;
    private OutputStream output;

    public PDFOverlayer(PDFOverlayConfiguration overlayConfig) {
        this.overlayConfig = overlayConfig;
    }

    public void setInput(byte[] input) {
        this.input = input;
    }

    public void setOutputStream(OutputStream output) {
        this.output = output;
    }

    public void process() throws IOException {
        List<PDFOverlayLayer> layers = overlayConfig.getLayers();
        List<PdfReader> overlayReaders = new ArrayList<>(layers.size());
        for (PDFOverlayLayer layer : layers) {
            overlayReaders.add(new PdfReader(layer.getPath()));
        }

        PdfReader document = new PdfReader(input);

        PdfStamper stamper = null;
        try {
            stamper = new PdfStamper(document, output);
            for (int iterPages = 0; iterPages < document.getNumberOfPages(); iterPages++) {
                for (int i = 0; i < layers.size(); i++) {
                    PDFOverlayLayer layer = layers.get(i);

                    PdfContentByte content;
                    if (PDFOverlayLayer.STACK_OVER.equals(layer.getStacked())) {
                        content = stamper.getOverContent(iterPages + 1);
                    } else {
                        content = stamper.getUnderContent(iterPages + 1);
                    }


                    PdfReader overlay = overlayReaders.get(i);

                    int overlayPage = findOverlayPage(layer.getPageOrder(), iterPages, overlay.getNumberOfPages());

                    if (overlayPage > 0) {
                        PdfImportedPage page = stamper.getImportedPage(overlay, overlayPage);
                        content.addTemplate(page,
                                layer.getScaleX(),
                                0,
                                0,
                                layer.getScaleY(),
                                layer.getPosX(),
                                layer.getPosY());
                    }
                }
            }
        } catch (DocumentException e) {
            throw new IOException(e);
        } finally {
            if (stamper != null) {
                try {
                    stamper.close();
                } catch (DocumentException | IOException e) {
                    log.warn(e.getLocalizedMessage(), e);
                }
            }
        }
    }

    private int findOverlayPage(String pageOrders, int documentPage, int overlayPages) {
        if (Utils.isEmpty(pageOrders)) {
            return 1;
        }

        String[] pageOrder = Utils.splitString(pageOrders);

        if ("mod".equalsIgnoreCase(pageOrder[0])) {
            return (documentPage % overlayPages) + 1;
        } else if ("default".equalsIgnoreCase(pageOrder[0])) {
            return 1;
        }

        if (documentPage >= pageOrder.length) {
            documentPage = pageOrder.length - 1;
        }

        return Integer.parseInt(pageOrder[documentPage], 10);
    }

    public static void main(String[] args) throws Exception {
        ByteArrayOutputStream pdfb = new ByteArrayOutputStream(102400);

        FileInputStream f = new FileInputStream("/Users/im/Downloads/SFFAH200012_128187_0pdf_1_.pdf");
        byte[] pdfBuf = new byte[2048];
        int read;

        while ((read = f.read(pdfBuf)) > -1) {
            pdfb.write(pdfBuf, 0, read);
        }
        byte[] pdf = pdfb.toByteArray();

        ByteArrayOutputStream baos = new ByteArrayOutputStream(102400);

        PDFOverlayConfiguration config = new PDFOverlayConfiguration();
        config.setId("sachb");
        PDFOverlayLayer layer = new PDFOverlayLayer();
        layer.setStacked("U");
        layer.setPath("/Users/im/Downloads/A4_Stugeba_Containersysteme_GmbH_2020_OPS.pdf");
        layer.setPageOrder("1");
        layer.setPosY(0);
        layer.setScaleY(1f);
        config.addLayer(layer);

        PDFOverlayer overlayer = new PDFOverlayer(config);
        overlayer.setOutputStream(baos);
        overlayer.setInput(pdf);
        overlayer.process();
        pdf = baos.toByteArray();
        baos.reset();


        /*
		config = new PDFOverlayConfiguration();
		config.setId("nca");
		layer = new PDFOverlayLayer();
		layer.setStacked("O");
		layer.setPath("/home/im/tmp/aba/Rechnungspapier-NCA.pdf");
		config.addLayer(layer);

		overlayer = new PDFOverlayer(config);
		overlayer.setOutputStream(baos);
		overlayer.setInput(pdf);
		overlayer.process();
		pdf = baos.toByteArray();
		baos.reset();
		*/


        FileOutputStream fos = new FileOutputStream("/Users/im/Downloads/out.pdf");
        fos.write(pdf);
        fos.close();
    }
}
