/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.pdf;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class DocumentTokens {
    private final Map<String, Set<String>> tokens = new TreeMap<>();
    private String user = null;
    private String mandant = null;
    private String aco = null;

    public StringBuilder content = new StringBuilder();

    protected DocumentTokens() {
    }

    /**
     * Adds an token. Initializes user and mandant is possible. Generates token "Rahmenauftrag" from "Auftragsnummer"
     *
     * @param token
     */
    protected void addToken(String token) {
        final String[] parts = token.split("=", 2);
        if (parts.length != 2) {
            throw new IllegalArgumentException("Invalid token: '" + token + "'");
        }

        final String key = parts[0];
        final String value = parts[1];

        addToken(key, value);
    }

    public void addToken(String key, String value) {
        if (key.equals("Benutzer")) {
            user = value;
            return;
        } else if (key.equals("Mandant")) {
            mandant = value;
            return;
        } else if (key.equals("ACO")) {
            aco = value;
            return;
        } else if (key.equals("Auftragsnummer")) {
            int pos = value.indexOf('-');
            if (pos > 0) {
                addToken("Rahmenauftrag", value.substring(0, pos));
            }
        }

        Set<String> values = tokens.computeIfAbsent(key, k -> new TreeSet<>());
        values.add(value);
    }

    public String getUser() {
        return user;
    }

    public String getMandant() {
        return mandant;
    }

    public String getACO() {
        return aco;
    }

    public Set<String> getTokenKeys() {
        return tokens.keySet();
    }

    @SuppressWarnings("unchecked")
    public Set<String> getTokenValues(String key) {
        Set<String> l = tokens.get(key);
        if (l != null) {
            return l;
        }

        return Collections.EMPTY_SET;
    }

    /**
     * @param token
     * @return the first value for this token found on any page
     */
    public String getTokenValue(String token) {
        for (String key : getTokenKeys()) {
            if (!key.equals(token)) {
                continue;
            }

            for (String value : getTokenValues(key)) {
                return value;
            }
        }

        return null;
    }

    public void addContent(String content) {
        if (content == null) {
            return;
        }

        this.content.append(content);
    }

    public String getContent() {
        return content.toString();
    }
}
