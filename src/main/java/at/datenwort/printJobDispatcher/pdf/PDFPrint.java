/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.pdf;

import at.datenwort.printJobDispatcher.config.PjdConfig;

import javax.print.PrintException;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;

public interface PDFPrint {
    class Accessor {
        public static PDFPrint get() {
            if ("pdfbox".equalsIgnoreCase(PjdConfig.getInstance().getPdfprint())) {
                return new PDFPrintPdfbox();
            } else if ("cmd".equalsIgnoreCase(PjdConfig.getInstance().getPdfprint())) {
                return new PDFPrintCmd(PjdConfig.getInstance().getPdfprintcmd());
            } else {
                return new PDFPrintPdfview();
            }
        }
    }

    void print(String printer, File pdfFile, String title, Integer copies, String tray, boolean monochrome) throws IOException, InterruptedException, PrintException, PrinterException;
}
