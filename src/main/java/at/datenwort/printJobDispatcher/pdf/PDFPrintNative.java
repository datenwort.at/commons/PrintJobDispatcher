/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.pdf;

import at.datenwort.printJobDispatcher.lib.Utils;
import at.datenwort.printJobDispatcher.lib.printer.PrinterDevice;
import at.datenwort.printJobDispatcher.lib.printer.PrinterEnvironment;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.Attribute;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Chromaticity;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.Media;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.PrinterName;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class PDFPrintNative implements PDFPrint {
    public void print(String printer, File pdfFile, String title, Integer copies, String tray, boolean monochrome) throws IOException, InterruptedException, PrintException, PrinterException {
        byte[] pdfBytes = Files.readAllBytes(pdfFile.toPath());

        AttributeSet aset = new HashPrintServiceAttributeSet();
        aset.add(new PrinterName(printer, null));

        DocFlavor flavor = DocFlavor.BYTE_ARRAY.PDF;
        PrintService[] pss = PrintServiceLookup.lookupPrintServices(flavor, aset);
        if (pss == null || pss.length < 1) {
            throw new IllegalArgumentException("printer " + printer + " not found");
        }
        PrintService ps = pss[0];

        final PrinterDevice pd = PrinterEnvironment.getPrinterEnvironment().findDevice(printer);
        if (pd == null) {
            throw new IllegalArgumentException("printer " + printer + " not found");
        }

        Attribute mediaTray = null;
        if (tray != null) {
            tray = pd.getTray(tray);

            Attribute[] medias = (Attribute[]) ps.getSupportedAttributeValues(Media.class, null, null);
            for (Attribute media : medias) {
                if (tray.equals(media.toString())) {
                    mediaTray = media;
                    break;
                }
            }

            if (mediaTray == null) {
                StringBuilder sb = new StringBuilder();
                for (Attribute media : medias) {
                    if (sb.length() > 0) {
                        sb.append(",");
                    }
                    sb.append(media.toString());
                }

                throw new UnsupportedOperationException("tray '" + tray + "' not available on printer '" + ps.getName() + "'. Supported trays:" + sb);
            }
        }


        PrintRequestAttributeSet praset = new HashPrintRequestAttributeSet();
        if (mediaTray != null) {
            praset.add(mediaTray);
        } else {
            praset.add(MediaSizeName.ISO_A4);
        }
        if (copies != null) {
            praset.add(new Copies(copies));
        }
        if (!Utils.isEmpty(title)) {
            praset.add(new JobName(title, null));
        }
        if (monochrome) {
            praset.add(Chromaticity.MONOCHROME);
        }

        DocPrintJob printJob = ps.createPrintJob();
        /*
        if (!Utils.isEmpty(title))
        {
            printJob.setJobName(title);
        }
        if (copies != null)
        {
            printJob.setCopies(copies);
        }
        printJob.setPrintService(ps);
        */

        Doc document = new SimpleDoc(pdfBytes, flavor, null);
        printJob.print(document, praset);
    }

    public static void main(String[] args) throws PrintException, IOException, PrinterException, InterruptedException {
        PDFPrint print = new PDFPrintNative();
        print.print("\\\\stgelo1\\PR0104",
                // new File("c:\\opsj\\spool\\1381926834-000000\\SFFA1300542_3_0pdf_5_.pdf"),
                new File("c:\\opsj\\spool\\1382621962-000000\\ZAZA130063_17494_0pdf_5_.pdf"),
                "test",
                1,
                null,
                false);
    }
}
