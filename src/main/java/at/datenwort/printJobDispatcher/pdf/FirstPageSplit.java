/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.pdf;

import java.util.Map;

public class FirstPageSplit implements PDFSplit.SplitActionInterface {
    private final Map<Integer, PageTokens> pageInfo;

    public FirstPageSplit(Map<Integer, PageTokens> pageInfo) {
        this.pageInfo = pageInfo;
    }

    public int splitAction(int pageNum) {
        if (pageInfo != null) {
            PageTokens pageInfoNum = pageInfo.get(pageNum);
            // if (pageInfoNum != null && pageInfoNum.getPageNumber() == 1)
            if (pageInfoNum != null) {
                PageTokens.LogicalPageInfo logicalPageInfo = pageInfoNum.getLogicalPageInfo();
                if (logicalPageInfo != null && logicalPageInfo.getDocumentPage() == 1) {
                    return PDFSplit.ACTION_SPLIT_PAGE;
                }
            }
        }

        return PDFSplit.ACTION_APPEND_PAGE;
    }
}
