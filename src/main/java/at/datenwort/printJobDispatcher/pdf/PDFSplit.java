/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.pdf;

import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;

/**
 * Split an document
 * <p/>
 * For every page, a splitAction is called, you could return one of ACTION constants.<br>
 * There is also an event interface, which allows you the take some action before/during/after document creation.
 *
 * @author Mario Ivankovits (mario@ops.co.at)
 * @see #ACTION_APPEND_PAGE
 * @see #ACTION_SPLIT_PAGE
 * @see #setSplitAction(PDFSplit.SplitActionInterface)
 * @see #setEventListener(PDFSplit.EventInterface)
 */
public class PDFSplit {
    public final static int ACTION_UNKNOWN = -1;

    /**
     * Append current page to current document
     */
    public final static int ACTION_APPEND_PAGE = 0;

    /**
     * Start a new document with current page
     */
    public final static int ACTION_SPLIT_PAGE = 1;

    private InputStream pdfDocumentStream;
    private String filename = null;

    private SplitActionInterface splitAction = null;
    private EventInterface eventInterface = null;

    /**
     * @see #splitAction(int)
     */
    public interface SplitActionInterface {
        /**
         * will be called for every page
         *
         * @param pageNum Number of current page
         * @see PDFSplit#ACTION_APPEND_PAGE
         * @see PDFSplit#ACTION_SPLIT_PAGE
         */
        int splitAction(int pageNum);
    }

    /**
     * @see #startDocument()
     * @see #addPage(int)
     * @see #documentCreated(String)
     */
    public interface EventInterface {
        /**
         * Will be called, if a new document is started.
         */
        void startDocument();

        /**
         * Will be called, if a new page has been added to the document.
         *
         * @param pageNum page number of the original document
         */
        void addPage(int pageNum);

        /**
         * Will be called, if a new document is finished.<br>
         * It the case, that the new document contains no pages,<br>
         * no document will be created, and therefore this event wont be fired.
         *
         * @param filename filename of the new document
         */
        void documentCreated(String filename);
    }

    public PDFSplit() {
    }

    public void setDocumentStream(InputStream input) {
        this.pdfDocumentStream = input;
    }

    public void process() throws IOException {
        int documentNum = 0;

        PDDocument currentDocument = null;
        OutputStream writerStream = null;
        String filenameOut = null;

        byte[] buf = pdfDocumentStream.readAllBytes();
        try (PDDocument input = Loader.loadPDF(buf)) {

            // PdfReader reader = new PdfReader(pdfDocumentStream);
            PDPageTree pages = input.getDocumentCatalog().getPages();
            for (int pageNum = 1; pageNum <= pages.getCount(); pageNum++) {
                int action = ACTION_SPLIT_PAGE;
                if (splitAction != null) {
                    action = splitAction.splitAction(pageNum);
                }
                if (currentDocument == null || action == ACTION_SPLIT_PAGE) {
                    documentNum++;

                    if (currentDocument != null) {
                        saveOutput(currentDocument, writerStream);

                        if (eventInterface != null) {
                            eventInterface.documentCreated(filenameOut);
                        }
                    }

                    if (eventInterface != null) {
                        eventInterface.startDocument();
                    }

                    currentDocument = new PDDocument();
                    currentDocument.setDocumentInformation(input.getDocumentInformation());
                    currentDocument.getDocumentCatalog().setViewerPreferences(input.getDocumentCatalog().getViewerPreferences());

                    filenameOut = MessageFormat.format(filename, documentNum);

                    writerStream = new FileOutputStream(filenameOut);
                }

                if (eventInterface != null) {
                    eventInterface.addPage(pageNum);
                }


                PDPage source = pages.get(pageNum - 1);
                PDPage imported = currentDocument.importPage(source);
                imported.setCropBox(source.getCropBox());
                imported.setMediaBox(source.getMediaBox());
                imported.setResources(source.getResources());
                imported.setRotation(source.getRotation());
            }

            if (currentDocument != null) {
                saveOutput(currentDocument, writerStream);

                if (eventInterface != null) {
                    eventInterface.documentCreated(filenameOut);
                }
            }
        }
    }

    private void saveOutput(PDDocument output, OutputStream outputStream) throws IOException {
        if (output == null) {
            return;
        }

        try (output) {
            output.save(outputStream);
        }
    }

    /**
     * set the output-filename.
     * {0} will be replace by the document number.
     *
     * @param filename e.g. /any/dir/new{0}.pdf
     */
    public void setOutputFileName(String filename) {
        this.filename = filename;
    }

    /**
     * Set your split-action
     *
     * @see PDFSplit.SplitActionInterface
     */
    public void setSplitAction(PDFSplit.SplitActionInterface splitAction) {
        this.splitAction = splitAction;
    }

    /**
     * Set your event listener
     *
     * @see PDFSplit.EventInterface
     */
    public void setEventListener(EventInterface eventListener) {
        this.eventInterface = eventListener;
    }

    /*
    public static void main(String[] args) throws Exception
	{
        PDFSplit split = new PDFSplit();
        split.setDocumentStream(new FileInputStream("c:\\postoffice\\temp\\70822926201112221226161.pdf"));
        split.setOutputFileName("c:/postoffice/temp/{0}.pdf");
        split.setSplitAction(new SplitActionInterface()
        {
            @Override
            public int splitAction(int pageNum)
            {
                if (pageNum <= 2)
                {
                    return PDFSplit.ACTION_APPEND_PAGE;
                }
                return PDFSplit.ACTION_SPLIT_PAGE;
            }
        });
        split.process();
	}
	*/
}
