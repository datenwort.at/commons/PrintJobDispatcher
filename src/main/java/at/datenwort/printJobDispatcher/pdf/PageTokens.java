/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.pdf;

import java.util.Set;
import java.util.TreeSet;

class PageTokens {
    private final int pagenum;
    private final Set<String> tokens = new TreeSet<>();
    private LogicalPageInfo logicalPageInfo = null;
    private final StringBuilder pageContent = new StringBuilder();

    public static class LogicalPageInfo {
        private final int documentPage;
        private final int documentPages;
        private final int reportPages;

        private LogicalPageInfo(final int documentPage, final int documentPages, final int reportPages) {
            this.documentPage = documentPage;
            this.documentPages = documentPages;
            this.reportPages = reportPages;
        }

        public int getDocumentPage() {
            return documentPage;
        }

        public int getDocumentPages() {
            return documentPages;
        }

        public int getReportPages() {
            return reportPages;
        }
    }

    protected PageTokens(int pagenum) {
        this.pagenum = pagenum;
    }

    public int getPageNumber() {
        return this.pagenum;
    }

    protected void addToken(String token) {
        if (token != null && token.startsWith(InfoFilePDFSplit.TOKEN_PAGE)) {
            processLogicalPageToken(token);
        }
        tokens.add(token);
    }

    private void processLogicalPageToken(String token) {
        String logicalPageInfo = token.substring(InfoFilePDFSplit.TOKEN_PAGE.length());
        String tokens[] = logicalPageInfo.split("/");

        int documentPage = -1;
        int documentPages = -1;
        int reportPages = -1;

        if (tokens.length > 0) {
            documentPage = Integer.parseInt(tokens[0], 10);
        }
        if (tokens.length > 1) {
            documentPages = Integer.parseInt(tokens[1], 10);
        }
        if (tokens.length > 2) {
            reportPages = Integer.parseInt(tokens[2], 10);
        }

        if (documentPage > -1) {
            this.logicalPageInfo = new LogicalPageInfo(documentPage, documentPages, reportPages);
        }
    }

    public void addContent(String docText) {
        if (docText == null) {
            return;
        }

        pageContent.append(docText);
        pageContent.append("\n");
    }

    public String getContent() {
        return pageContent.toString();
    }

    public Set<String> getTokens() {
        return tokens;
    }

    public LogicalPageInfo getLogicalPageInfo() {
        return logicalPageInfo;
    }
}
