/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.pdf;

import at.datenwort.printJobDispatcher.lib.Utils;
import at.datenwort.printJobDispatcher.lib.printer.PrinterDevice;
import at.datenwort.printJobDispatcher.lib.printer.PrinterEnvironment;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.printing.PDFPageable;

import javax.print.DocFlavor;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.attribute.Attribute;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Chromaticity;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.Media;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.PrinterName;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;

public class PDFPrintPdfbox implements PDFPrint {
    public void print(String printer, File pdfFile, String title, Integer copies, String tray, boolean monochrome) throws IOException, InterruptedException, PrintException, PrinterException {
        AttributeSet aset = new HashPrintServiceAttributeSet();
        aset.add(new PrinterName(printer, null));
        PrintService[] pss = javax.print.PrintServiceLookup.lookupPrintServices(DocFlavor.SERVICE_FORMATTED.PAGEABLE, aset);

        if (pss == null || pss.length < 1) {
            throw new IllegalArgumentException("printer " + printer + " not found");
        }
        PrintService ps = pss[0];

        final PrinterDevice pd = PrinterEnvironment.getPrinterEnvironment().findDevice(printer);
        if (pd == null) {
            throw new IllegalArgumentException("printer " + printer + " not found");
        }

        try (PDDocument pdffile = Loader.loadPDF(pdfFile)) {
            Attribute mediaTray = null;
            if (tray != null) {
                tray = pd.getTray(tray);

                Attribute[] medias = (Attribute[]) ps.getSupportedAttributeValues(Media.class, null, null);
                for (Attribute media : medias) {
                    if (tray.equals(media.toString())) {
                        mediaTray = media;
                        break;
                    }
                }

                if (mediaTray == null) {
                    StringBuilder sb = new StringBuilder();
                    for (Attribute media : medias) {
                        if (sb.length() > 0) {
                            sb.append(",");
                        }
                        sb.append(media.toString());
                    }

                    throw new UnsupportedOperationException("tray '" + tray + "' not available on printer '" + ps.getName() + "'. Supported trays:" + sb);
                }
            }

            // derive orientation from first page
            PDPage page = pdffile.getNumberOfPages() > 0 ? pdffile.getDocumentCatalog().getPages().get(0) : null;
            DocAttributeSet daset = new HashDocAttributeSet();
            if (page != null && (page.getRotation() == 90 || page.getRotation() == 270)) {
                daset.add(OrientationRequested.LANDSCAPE);
            } else {
                daset.add(OrientationRequested.PORTRAIT);
            }

            PrintRequestAttributeSet praset = new HashPrintRequestAttributeSet();
            if (mediaTray != null) {
                praset.add(mediaTray);
            } else {
                praset.add(MediaSizeName.ISO_A4);
            }
            if (copies != null) {
                praset.add(new Copies(copies));
            }
            if (!Utils.isEmpty(title)) {
                praset.add(new JobName(title, null));
            }
            if (monochrome) {
                praset.add(Chromaticity.MONOCHROME);
            }

            PrinterJob printJob = PrinterJob.getPrinterJob();
            if (!Utils.isEmpty(title)) {
                printJob.setJobName(title);
            }
            if (copies != null) {
                printJob.setCopies(copies);
            }
            printJob.setPrintService(ps);

            printJob.setPageable(new PDFPageable(pdffile));
            printJob.print(praset);
        }
    }

    public static void main(String[] args) throws PrintException, IOException, PrinterException, InterruptedException {
        PDFPrint print = new PDFPrintPdfbox();
        print.print("Kyocera_ECOSYS_M5526cdw",
                new File("/Users/im/Downloads/data.pdf"),
                "test",
                1,
                null,
                false);
    }
}
