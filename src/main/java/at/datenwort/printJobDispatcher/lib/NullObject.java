/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.lib;

import java.io.Serializable;

public class NullObject implements Serializable, Comparable<Object> {
    public final static NullObject INSTANCE = new NullObject();

    public final static String NULL = "null";

    private NullObject() {
    }

    public String toString() {
        return NULL;
    }

    /**
     * Vergleicht, ob ein NullObject gleich ist mit
     * 1) einem String in dem <code>NullObject.NULL</code> steht
     * 2) oder das Object <code>instanceof NullObject</code> ist
     */
    public boolean equals(Object o) {
        if (o instanceof String) {
            return o.equals(NULL);
        }
        return o instanceof NullObject;
    }

    public static boolean mightBeNull(Object o) {
        return INSTANCE.equals(o);
    }

    public int compareTo(Object o) {
        if (this == o) {
            return 0;
        }

        return -1;
    }
}
