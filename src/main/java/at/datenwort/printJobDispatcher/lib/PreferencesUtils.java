/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.lib;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Created by IntelliJ IDEA.
 * User: im
 * Date: 12.08.2009
 * Time: 14:23:47
 * To change this template use File | Settings | File Templates.
 */
public final class PreferencesUtils {
    private PreferencesUtils() {
    }

    public static Preferences nodeIfExists(Preferences node, String nodeName) throws BackingStoreException {
        if (!node.nodeExists(nodeName)) {
            return null;
        }

        return node.node(nodeName);
    }

    public static String get(Preferences pref, String key, String def) {
        String text = pref.get(key, null);
        if (text != null && text.length() > 0) {
            return text;
        }

        return def;
    }

    public static boolean get(Preferences pref, String key, boolean def) {
        String text = pref.get(key, null);
        if (Utils.isEmpty(text)) {
            return def;
        }

        return Utils.guessBool(text);
    }
}
