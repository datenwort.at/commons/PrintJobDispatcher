/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.lib.collections;

import at.datenwort.printJobDispatcher.lib.NullObject;
import org.apache.commons.collections4.map.LRUMap;

import java.util.ArrayList;

/**
 * A list which allows to access a specific entry by its id. The entry has to provide a getId() method or subclas this
 * list and override {@link #getId}.<br />
 * It is allowed to change the id of an entry.
 */
public class IdList<E> extends ArrayList<E> {
    private static final long serialVersionUID = -3383804821590936789L;

    private transient LRUMap<Object, Object> idCache;

    public interface IdListEntry {
        String getId();

        void setId(String id);
    }

    protected Object getId(E element) {
        if (element instanceof IdListEntry) {
            return ((IdListEntry) element).getId();
        } else {
            return getId(element);
        }
    }

    protected Object _getId(E element) {
        throw new UnsupportedOperationException("element needs to implement IdListEntry interface or you have to override IdList.getId()");
    }

    public LRUMap<Object, Object> getIdCache() {
        if (idCache == null) {
            idCache = new LRUMap<>(30);
        }

        return idCache;
    }

    /**
     * get an entry by its id. The entry will be cached so that any further direct access will be faster.
     */
    public E getById(Object id) {
        Object element = getIdCache().get(id);
        // see if we have a hit and if the id still matches
        if (element != null && (NullObject.INSTANCE.equals(element) || id.equals(getId((E) element)))) {
            return NullObject.INSTANCE.equals(element) ? null : (E) element;
        }

        for (E backerElement : this) {
            if (id.equals(getId(backerElement))) {
                getIdCache().put(id, backerElement);
                return backerElement;
            }
        }

        getIdCache().put(id, NullObject.INSTANCE);
        return null;
    }
}
