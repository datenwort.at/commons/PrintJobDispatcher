/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.lib.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;
import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

public class FileSystemPreferencesFactory implements PreferencesFactory {

    private final Properties properties;

    public FileSystemPreferencesFactory() {
        this.properties = new Properties();

        this.load();
    }

    @Override
    public Preferences systemRoot() {
        return new FileSystemPreferencesPropertiesAdapter(this, null, "");
    }

    @Override
    public Preferences userRoot() {
        return systemRoot();
    }

    void set(String key, String value) {
        properties.setProperty(key, value);
        save();
    }

    String get(String key) {
        return properties.getProperty(key);
    }

    public void remove(String keyToRemove) {
        final String keyToRemoveTree = keyToRemove + ".";

        for (String key : properties.stringPropertyNames()) {
            if (key.equals(keyToRemove) || key.startsWith(keyToRemoveTree)) {
                properties.remove(key);
            }
        }

        save();
    }

    public String[] keys(String keyToFind) {
        final String keyToFindTree = keyToFind.length() > 0 ? keyToFind + "." : "";

        Set<String> ret = new LinkedHashSet<>();

        for (String key : properties.stringPropertyNames()) {
            if (keyToFindTree.length() == 0 || key.startsWith(keyToFindTree)) {
                key = key.substring(keyToFindTree.length());
                int pos = key.indexOf(".");
                if (pos < 0) {
                    ret.add(key);
                } else {
                    ret.add(key.substring(0, pos));
                }
            }
        }

        return ret.toArray(new String[0]);
    }

    public String[] children(String keyToFind) {
        final String keyToFindTree = keyToFind.length() > 0 ? keyToFind + "." : "";

        Set<String> ret = new LinkedHashSet<>();

        for (String key : properties.stringPropertyNames()) {
            if (keyToFindTree.length() == 0 || key.startsWith(keyToFindTree)) {
                key = key.substring(keyToFindTree.length());
                int pos = key.indexOf(".");
                if (pos > -1) {
                    key = key.substring(0, pos);
                }
                if (properties.containsKey(keyToFindTree + key)) {
                    // value node
                    continue;
                }

                ret.add(key);
            }
        }

        return ret.toArray(new String[0]);
    }

    public void save() {
        try {
            Path config = Paths.get("config", "PJD.xml");
            if (!Files.exists(config)) {
                Files.createDirectories(config.getParent());
            }

            try (OutputStream stream = Files.newOutputStream(config, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
                properties.storeToXML(stream, "Config", StandardCharsets.UTF_8);
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void load() {
        try {
            Path config = Paths.get("config", "PJD.xml");
            if (!Files.exists(config)) {
                Files.createDirectories(config.getParent());
            } else {
                try (InputStream inputStream = Files.newInputStream(config)) {
                    properties.loadFromXML(inputStream);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
