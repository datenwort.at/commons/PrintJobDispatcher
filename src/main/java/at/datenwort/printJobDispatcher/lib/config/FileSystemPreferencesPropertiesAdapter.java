/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.lib.config;

import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;

public class FileSystemPreferencesPropertiesAdapter extends AbstractPreferences {
    private final FileSystemPreferencesFactory factory;

    protected FileSystemPreferencesPropertiesAdapter(FileSystemPreferencesFactory factory, AbstractPreferences parent, String name) {
        super(parent, name);
        this.factory = factory;
    }

    private String join(String parent, String key) {
        if (parent == null) {
            return key;
        }
        return parent + "." + key;
    }

    @Override
    protected void putSpi(String key, String value) {
        factory.set(join(name(), key), value);
    }

    @Override
    protected String getSpi(String key) {
        return factory.get(join(name(), key));
    }

    @Override
    protected void removeSpi(String key) {
        factory.remove(join(name(), key));
    }

    @Override
    protected void removeNodeSpi() throws BackingStoreException {
        factory.remove(name());
    }

    @Override
    protected String[] keysSpi() throws BackingStoreException {
        return factory.keys(name());
    }

    @Override
    protected String[] childrenNamesSpi() throws BackingStoreException {
        return factory.children(name());
    }

    @Override
    protected AbstractPreferences childSpi(String name) {
        FileSystemPreferencesPropertiesAdapter ret = new FileSystemPreferencesPropertiesAdapter(factory, this, name);
        return ret;
    }

    @Override
    protected void syncSpi() throws BackingStoreException {
        factory.save();
        factory.load();
    }

    @Override
    protected void flushSpi() throws BackingStoreException {
        factory.save();
    }
}
