/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.lib.printer;

import at.datenwort.printJobDispatcher.lib.Utils;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.JobName;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.prefs.Preferences;

public class JavaPrinterDevice implements PrinterDevice {
    private final PrintService printService;
    private final String name;
    private final Preferences prefs;

    private String title;

    private File m_file = null;
    private OutputStream m_of = null;
    private Writer m_osw = null;

    private String encoding = null;

    private OutputStream m_out = null;

    private int relativeMarginX;
    private int relativeMarginY;
    private int relativeMarginW;
    private int relativeMarginH;

    protected JavaPrinterDevice(PrintService printService, String name, Preferences prefs) {
        this.printService = printService;
        this.name = name;
        this.prefs = prefs;
    }

    void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return printService.getName();
    }

    public PrintService getPrintService() {
        return printService;
    }

    public Writer openText() throws IOException {
        if (m_of == null) {
            m_file = File.createTempFile("opsjprint", ".prn");
            m_of = new FileOutputStream(m_file);
        }
        if (m_osw == null) {
            if (encoding != null) {
                m_osw = new OutputStreamWriter(m_of, encoding);
            } else {
                m_osw = new OutputStreamWriter(m_of);
            }
        }

        return m_osw;
    }

    public OutputStream openBinary() throws IOException {
        if (m_of == null) {
            m_file = File.createTempFile("opsjprint", ".prn");
            m_of = new FileOutputStream(m_file);
        }

        return m_of;
    }

    public void close() throws IOException, PrintException {
        try {
            if (m_out != null) {
                m_out.close();
                m_out = null;
            }
            if (m_osw != null) {
                m_osw.close();
                m_osw = null;
            }

            if (m_of != null) {
                m_of.close();

                try {
                    spoolFile(m_file);
                } finally {
                    m_file.delete();
                    m_of = null;
                }
            }
        } finally {
            title = null;
        }
    }

    public void spoolFile(File file) throws IOException, PrintException {
        DocPrintJob job = printService.createPrintJob();

        DocAttributeSet das = new HashDocAttributeSet();

        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        if (!Utils.isEmpty(getTitle())) {
            pras.add(new JobName(getTitle(), null));
        } else {
            pras.add(new JobName(file.getName(), null));
        }

        Doc doc = new SimpleDoc(new FileInputStream(file), DocFlavor.INPUT_STREAM.AUTOSENSE, das);
        job.print(doc, pras);
    }

    public boolean isClosed() {
        return m_of == null;
    }

    public void setRelativeMargins(int x, int y, int w, int h) {
        relativeMarginX = x;
        relativeMarginY = y;
        relativeMarginW = w;
        relativeMarginH = h;
    }

    public int getRelativeMarginX() {
        return relativeMarginX;
    }

    public int getRelativeMarginY() {
        return relativeMarginY;
    }

    public int getRelativeMarginW() {
        return relativeMarginW;
    }

    public int getRelativeMarginH() {
        return relativeMarginH;
    }

    @Override
    public String getTray(String logicalName) {
        return prefs.node("trays").get(logicalName, logicalName);
    }
}
