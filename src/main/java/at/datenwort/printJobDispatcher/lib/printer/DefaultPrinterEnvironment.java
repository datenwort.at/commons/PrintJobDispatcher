/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.lib.printer;

import at.datenwort.printJobDispatcher.lib.EnvironmentAccess;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.print.PrintService;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

public class DefaultPrinterEnvironment extends PrinterEnvironment {
    private final static Log log = LogFactory.getLog(DefaultPrinterEnvironment.class);

    public DefaultPrinterEnvironment() {
        super();
    }

    public PrinterDevice findDevice(String sName) {
        if (sName == null) {
            return null;
        }

        PrinterDevice[] devices = getPrinterDevices();
        if (devices == null) {
            return null;
        }

        // Normal suchen
        for (at.datenwort.printJobDispatcher.lib.printer.PrinterDevice pd : devices) {
            if (pd.getName().equalsIgnoreCase(sName) || pd.getName().equalsIgnoreCase(sName.replace('_', ' '))) {
                return pd;
            }
        }

        int pos = sName.lastIndexOf("/");
        if (pos < 0) {
            pos = sName.lastIndexOf("\\");
        }
        if (pos > -1) {
            sName = sName.substring(pos + 1);

            for (PrinterDevice pd : devices) {
                if (pd.getName().equalsIgnoreCase(sName)) {
                    return pd;
                }
            }
        }

        return null;
    }

    public PrinterDevice[] getPrinterDevices() {
        synchronized (DefaultPrinterEnvironment.class) {
            PrinterDevice[] devices;

            // The list of print-services might change, honor this and refetch the print-services
            // each time
            // if (m_devices == null)
            {
                log.trace("enumerating print services");

                List<PrinterDevice> printerDevices = new ArrayList<>();
                PrintService[] pss = javax.print.PrintServiceLookup.lookupPrintServices(null, null);
                for (PrintService ps : pss) {
                    log.trace("found printer service " + ps.getName());

                    String name = ps.getName();
                    int networkPrinterNamePos = name.lastIndexOf('\\');
                    if (networkPrinterNamePos > -1) {
                        name = name.substring(networkPrinterNamePos + 1);
                    }

                    name = name.replaceAll("[\\\\/]", "_");

                    Preferences prefs;
                    try {
                        prefs = EnvironmentAccess.get().node("printers").node(name);
                    } catch (Exception e) {
                        log.warn("Problems accessing printers node for printer '" + name + "' - Ignoring printer.");
                        continue;
                    }

                    JavaPrinterDevice pd = new JavaPrinterDevice(ps, name, prefs);

                    pd.setEncoding(prefs.get("encoding", null));

                    String margins = prefs.get("relative_margins", null);
                    if (margins != null && margins.length() > 0) {
                        int[] margin = new int[4];
                        String[] marginTokens = margins.split(",", 4);
                        for (int i = 0;
                             i < marginTokens.length;
                             i++) {
                            margin[i] = Integer.parseInt(marginTokens[i], 10);
                        }

                        pd.setRelativeMargins(margin[0], margin[1], margin[2], margin[3]);
                    }

                    printerDevices.add(pd);
                }

                devices = printerDevices.toArray(new PrinterDevice[0]);
            }

            return devices;
        }
    }
}
