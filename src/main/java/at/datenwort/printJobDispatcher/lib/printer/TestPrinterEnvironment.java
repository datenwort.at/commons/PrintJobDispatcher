/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.lib.printer;

import java.util.HashMap;
import java.util.Map;

public class TestPrinterEnvironment extends PrinterEnvironment {
    private Map<String, PrinterDevice> devices = new HashMap<>();

    public TestPrinterEnvironment() {
        super();
    }

    public PrinterDevice findDevice(String sName) {
        PrinterDevice pd = devices.get(sName);
        if (pd == null) {
            pd = new TestPrinterDevice(sName);
            devices.put(sName, pd);
        }

        return pd;
    }

    public PrinterDevice[] getPrinterDevices() {
        return devices.values().toArray(new PrinterDevice[devices.size()]);
    }
}
