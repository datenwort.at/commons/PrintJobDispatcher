/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.lib.printer;

import javax.print.PrintException;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

public interface PrinterDevice {
    String getName();

    String getDescription();

    Writer openText() throws IOException;

    OutputStream openBinary() throws IOException;

    void close() throws IOException, PrintException;

    void spoolFile(File file) throws IOException, PrintException;

    boolean isClosed();

    int getRelativeMarginX();

    int getRelativeMarginY();

    int getRelativeMarginW();

    int getRelativeMarginH();

    String getTray(String logicalName);
}
