/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.lib.printer;

import javax.activation.DataSource;
import javax.print.PrintException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestPrinterDevice implements PrinterDevice {
    private final static Logger log = Logger.getLogger(TestPrinterDevice.class.getName());

    private final String name;

    private String title;

    private File m_file = null;
    private FileOutputStream m_of = null;
    private Writer m_osw = null;

    private String encoding = null;

    private PrintWriter m_printWriter = null;
    private OutputStream m_out = null;

    private int relativeMarginX;
    private int relativeMarginY;
    private int relativeMarginW;
    private int relativeMarginH;

    protected TestPrinterDevice(String name) {
        this.name = name;
    }

    public String getEncoding() {
        return encoding;
    }

    void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return getName();
    }

    public Writer openText() throws IOException {
        if (m_of == null) {
            m_file = File.createTempFile("TESTPRINT_" + getName() + "-", ".txt");
            log.log(Level.INFO, "Created print file {0}", m_file.getAbsolutePath());
            m_of = new FileOutputStream(m_file);
        }
        if (m_osw == null) {
            if (encoding != null) {
                m_osw = new OutputStreamWriter(m_of, encoding);
            } else {
                m_osw = new OutputStreamWriter(m_of);
            }
        }

        return m_osw;
    }

    public OutputStream openBinary() throws IOException {
        if (m_of == null) {
            m_file = File.createTempFile("TESTPRINT_" + getName() + "-", ".bin");
            log.log(Level.INFO, "Created print file {0}", m_file.getAbsolutePath());

            m_of = new FileOutputStream(m_file);
        }

        return m_of;
    }

    public void finishDevice() throws IOException, PrintException {
        if (!isClosed()) {
            close();
        }
    }

    public void close() throws IOException, PrintException {
        try {
            closePrintWriter();

            if (m_out != null) {
                m_out.close();
                m_out = null;
            }
            if (m_osw != null) {
                m_osw.close();
                m_osw = null;
            }

            if (m_of != null) {
                m_of.close();

                try {
                    spoolFile(m_file);
                } finally {
                    m_file.delete();
                    m_of = null;
                }
            }
        } finally {
            title = null;
        }
    }

    public void spoolFile(File file) throws IOException, PrintException {
    }

    public boolean isClosed() {
        return m_of == null;
    }

    /**
     * We assume the DataSource knows what to print, so print all in binary mode.
     * So see this like a data pump
     */
    public void print(DataSource dataSource) throws IOException, PrintException {
        OutputStream osw;
        InputStream is = null;

        try {
            int bufLen = 10240;
            byte[] buf = new byte[bufLen];

            osw = openBinary();
            is = dataSource.getInputStream();
            int bufRead;
            while ((bufRead = is.read(buf, 0, bufLen)) > 0) {
                osw.write(buf, 0, bufRead);
            }

            osw.flush();
        } finally {
            try {
                if (is != null) {
                    is.close();
                    is = null;
                }
            } catch (Throwable t) {
                log.log(Level.WARNING, "Problems closing input stream", t);
            }

            close();
        }
    }

    public void println(String sString) throws IOException {
        PrintWriter pw = getPrintWriter();
        pw.println(sString);
        pw.flush();
    }

    private PrintWriter getPrintWriter() throws IOException {
        if (m_printWriter != null) {
            return m_printWriter;
        }

        m_printWriter = new PrintWriter(openText());
        return m_printWriter;
    }

    protected void closePrintWriter() throws IOException {
        if (m_printWriter != null) {
            m_printWriter.flush();
        }

        m_printWriter = null;
    }

    public void setRelativeMargins(int x, int y, int w, int h) {
        relativeMarginX = x;
        relativeMarginY = y;
        relativeMarginW = w;
        relativeMarginH = h;
    }

    public int getRelativeMarginX() {
        return relativeMarginX;
    }

    public int getRelativeMarginY() {
        return relativeMarginY;
    }

    public int getRelativeMarginW() {
        return relativeMarginW;
    }

    public int getRelativeMarginH() {
        return relativeMarginH;
    }

    @Override
    public String getTray(String logicalName) {
        return logicalName;
    }
}
