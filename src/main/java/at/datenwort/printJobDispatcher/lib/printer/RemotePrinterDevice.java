/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.lib.printer;

import at.datenwort.printJobDispatcher.lib.EnvironmentAccess;
import at.datenwort.printJobDispatcher.lib.FileUtils;

import javax.print.PrintException;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class RemotePrinterDevice implements PrinterDevice {
    private final String remoteHost;
    private final int remotePort;
    private final File remoteQueue;
    private final String printerName;

    private final static DateTimeFormatter dirNameFormat = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss_SSS");

    private QueueDir currentSpoolDir;

    private Writer writer;
    private OutputStream stream;

    private static class QueueDir {
        private final File file;
        private final String queueDir;

        public QueueDir(File dir, String queueDir) {
            this.file = dir;
            this.queueDir = queueDir;
        }
    }

    public RemotePrinterDevice(String remoteHost, int remotePort, File remoteQueue, String printerName) {
        this.remoteHost = remoteHost;
        this.remotePort = remotePort;
        this.remoteQueue = remoteQueue;
        this.printerName = printerName;
    }

    @Override
    public String getName() {
        return printerName;
    }

    @Override
    public String getDescription() {
        return printerName;
    }

    protected File getOutput(String filename) {
        if (currentSpoolDir == null) {
            currentSpoolDir = createQueueDir();
        }

        return new File(currentSpoolDir.file, filename);
    }

    private QueueDir createQueueDir() {
        String dirName = dirNameFormat.format(LocalDateTime.now());

        int dupkey = 0;
        while (dupkey < 999) {
            String name = String.format("%s-%03d", dirName, dupkey);
            File dir = new File(remoteQueue, name);
            if (dir.mkdirs()) {
                return new QueueDir(dir, name);
            }
            dupkey++;
        }

        throw new IllegalStateException("Druckjob konnte nicht angelegt werden. Basisverzeichnis=" + remoteQueue.getAbsolutePath() + " Verzeichnis=" + dirName);
    }

    @Override
    public Writer openText() throws IOException {
        if (writer != null) {
            return writer;
        }

        File output = getOutput("data.txt");
        writer = new OutputStreamWriter(new FileOutputStream(output, false), StandardCharsets.UTF_8);
        return writer;
    }

    @Override
    public OutputStream openBinary() throws IOException {
        if (stream != null) {
            return stream;
        }

        File output = getOutput("data.bin");
        stream = new FileOutputStream(output, false);
        return stream;
    }

    @Override
    public void close() throws IOException, PrintException {
        boolean print = false;
        if (writer != null) {
            try {
                writer.close();
            } finally {
                writer = null;
            }
            print = true;
        }

        if (stream != null) {
            try {
                stream.close();
            } finally {
                stream = null;
            }
            print = true;
        }

        if (print) {
            issuePrintCommand();
        }
    }

    protected void issuePrintCommand() throws IOException {
        try (PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(getOutput("config"), false), StandardCharsets.UTF_8))) {
            pw.println(String.format("%s=%s", "printer", printerName));
            pw.println(String.format("%s=%s", "fuser", EnvironmentAccess.getUserName()));
            pw.println(String.format("%s=%s", "passthrough", "true"));
        }

        try (Socket socket = new Socket(remoteHost, remotePort);
             BufferedOutputStream baos = new BufferedOutputStream(socket.getOutputStream())) {
            baos.write(("job " + currentSpoolDir.queueDir).getBytes(StandardCharsets.UTF_8));
        }
    }

    @Override
    public void spoolFile(File file) throws IOException {
        String filename = file.getName().toLowerCase();
        if (!filename.endsWith(".pdf")) {
            throw new UnsupportedOperationException("Es können nur PDF-Dateien gedruckt werden. Angeforderte Datei: " + filename);
        }

        File output = getOutput("data.pdf");

        FileUtils.copy(file, output);

        issuePrintCommand();
    }

    @Override
    public boolean isClosed() {
        return writer == null && stream == null;
    }

    @Override
    public int getRelativeMarginX() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getRelativeMarginY() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getRelativeMarginW() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getRelativeMarginH() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getTray(String logicalName) {
        throw new UnsupportedOperationException();
    }
}
