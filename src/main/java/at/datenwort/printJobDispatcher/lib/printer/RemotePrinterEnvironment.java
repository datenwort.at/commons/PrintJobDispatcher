/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.lib.printer;

import at.datenwort.printJobDispatcher.lib.EnvironmentAccess;

import java.io.File;

public class RemotePrinterEnvironment extends PrinterEnvironment {
    public RemotePrinterEnvironment() {
        super();
    }

    public PrinterDevice findDevice(String sName) {
        int pos = sName.lastIndexOf("/");
        if (pos < 0) {
            pos = sName.lastIndexOf("\\");
        }
        if (pos > -1) {
            sName = sName.substring(pos + 1);
        }

        File remoteQueue = new File(EnvironmentAccess.get().node("printers").get("remote_queue", "/remoteQueue"));
        String remoteHost = EnvironmentAccess.get().node("printers").get("remote_host", "localhost");
        int remotePort = Integer.parseInt(EnvironmentAccess.get().node("printers").get("remote_port", "3250"), 10);

        return new RemotePrinterDevice(remoteHost, remotePort, remoteQueue, sName);
    }

    public PrinterDevice[] getPrinterDevices() {
        throw new UnsupportedOperationException();
    }
}
