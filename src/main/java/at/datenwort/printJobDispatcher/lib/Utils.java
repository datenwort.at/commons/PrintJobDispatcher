/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.lib;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by mario.ivankovits on 14.10.13.
 */
public final class Utils {
    private Utils() {
    }

    public static <T> T[] asArray(T... elements) {
        return elements;
    }

    public static boolean isEmpty(String text) {
        if (text == null) {
            return true;
        }

        return text.trim().isEmpty();
    }

    public static <E> E[] toArray(final Class<E> componentType, final Collection<E> collection) {
        E[] array = (E[]) Array.newInstance(componentType, collection.size());
        return collection.toArray(array);
    }

    public static List<String> split(char c, String s) {
        if (s == null) {
            return null;
        }

        ArrayList<String> al = new ArrayList<>();

        int pos = -1;
        int lastPos = 0;

        do {
            pos = s.indexOf(c, pos + 1);
            if (pos < 0) {
                continue;
            }

            if (lastPos != pos) {
                al.add(s.substring(lastPos, pos));
            }
            lastPos = pos + 1;
        }
        while (pos > -1);

        if (lastPos < s.length()) {
            al.add(s.substring(lastPos));
        }

        return al;
    }

    public static String[] splitString(String text) {
        if (text == null) {
            return null;
        }

        return text.split(" *[,;] *");
    }

    public static <E> E getFirstNonNull(E... objs) {
        if (objs == null) {
            return null;
        }

        for (E obj : objs) {
            if (obj != null) {
                return obj;
            }
        }

        return null;
    }

    public static boolean guessBool(String str) {
        return guessBool(str, false);
    }

    public static boolean guessBool(String str, boolean bTextOnly) {
        if (str != null
                && str.length() != 0
                && ("true".equalsIgnoreCase(str) // NON-NLS
                || (!bTextOnly && ("1".equals(str) || "1.0".equals(str))) // NON-NLS
                || "ja".equalsIgnoreCase(str) // NON-NLS
                || "yes".equalsIgnoreCase(str) // NON-NLS
                || "j".equalsIgnoreCase(str) // NON-NLS
                || "on".equalsIgnoreCase(str))) // NON-NLS
        {
            return true;
        }

        return false;
    }

    /**
     * vergleich 2 strings unter ruechsichtnahme auf null values (null ist dabei
     * kleiner als jeder string ungleich null)
     */
    public static int compare(Comparable s1, Comparable s2) {
        if (s1 == null && s2 == null) {
            return 0;
        } else if (s1 == null) {
            return -1;
        } else if (s2 == null) {
            return 1;
        }
        return s1.compareTo(s2);
    }

    public static String join(String d, Object... s) {
        if (s == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder(512);
        for (int i = 0; i < s.length; i++) {
            if (s[i] == null) {
                continue;
            }

            if (sb.length() > 0) {
                sb.append(d);
            }

            sb.append(s[i].toString().trim());
        }

        return sb.toString();
    }

    public static <T> T getFirstOrNull(T... values) {
        if (values == null || values.length < 1) {
            return null;
        }

        return values[0];
    }

    public static String getEnvDef(String name, String def) {
        String value = System.getProperty(name);
        if (value == null) {
            value = System.getenv(name);
        }

        if (value == null) {
            return def;
        }

        return value;
    }
}
