/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.lib;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.concurrent.TimeUnit;

public final class FileUtils {
    private final static Log log = LogFactory.getLog(FileUtils.class);

    private FileUtils() {
    }

    public static void copyTo(File file, OutputStream os) throws IOException {
        byte[] buf = new byte[8192];

        try (FileInputStream fis = new FileInputStream(file)) {

            int read;
            while ((read = fis.read(buf)) > -1) {
                os.write(buf, 0, read);
            }
        }
    }

    public static FileFilter getFileFileFilter() {
        return File::isFile;
    }

    public static FileFilter getDirectoryFileFilter() {
        return File::isDirectory;
    }

    public static void copy(File src, File dst) throws FileNotFoundException {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            fis = new FileInputStream(src);
            fos = new FileOutputStream(dst);
            copy(fis, fos);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    log.warn("Problems closing file " + src.getAbsolutePath(), e);
                }
            }

            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    log.warn("Problems closing file " + dst.getAbsolutePath(), e);
                }
            }
        }
    }

    public static void copy(Reader reader, Writer writer) {
        char[] buf = new char[8192];

        try {
            int read;
            while ((read = reader.read(buf)) > -1) {
                writer.write(buf, 0, read);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void copy(InputStream inputStream, OutputStream outputStream) {
        byte[] buf = new byte[8192];

        try {
            int read;
            while ((read = inputStream.read(buf)) > -1) {
                outputStream.write(buf, 0, read);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void closeQuietly(InputStream inputStream) {
        try {
            inputStream.close();
        } catch (IOException e) {
            log.warn(e.getLocalizedMessage(), e);
        }
    }

    public static void closeQuietly(OutputStream outputStream) {
        try {
            outputStream.close();
        } catch (IOException e) {
            log.warn(e.getLocalizedMessage(), e);
        }
    }

    public static byte[] toByteArray(InputStream inputStream) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        copy(inputStream, baos);
        closeQuietly(baos);

        return baos.toByteArray();
    }

    public static void deleteQuietly(File file) {
        file.delete();
    }

    public static String getExtension(File file) {
        if (file == null) {
            return null;
        }

        String name = file.getName();
        if (Utils.isEmpty(name)) {
            return null;
        }

        int pos = name.lastIndexOf(".");
        if (pos < 0) {
            return null;
        }

        return name.substring(pos + 1);
    }

    public static String read(Path path, Charset charset) throws IOException {
        return Files.readString(path, charset);
    }

    public static FileTime toUnit(FileTime fileTime, TimeUnit unit) {
        if (fileTime == null) {
            return fileTime;
        }

        return FileTime.from(fileTime.to(unit), unit);
    }
}
