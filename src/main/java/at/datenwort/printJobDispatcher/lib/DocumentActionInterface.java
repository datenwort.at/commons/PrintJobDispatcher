/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPS-J (c) 2004 OPS EDV. All Rights Reserved.
 */
package at.datenwort.printJobDispatcher.lib;

import at.datenwort.printJobDispatcher.dblib.DocumentInterface;
import at.datenwort.printJobDispatcher.server.processes.ProcessLogger;

import java.io.File;
import java.util.prefs.Preferences;

public interface DocumentActionInterface {
    void process(ProcessLogger logger, DocumentInterface doc, File pdfFile) throws Exception;

    Object getConfig();

    void initializePlugin(Preferences prefs);
}
