/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.lib;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class RegexSubstitutor {
    // This class is necessary because the Matcher.groupCount() method
    // did not seem to work.
    protected static class MatchInfo {
        public final int start;
        public final int end;
        public final String group;

        protected MatchInfo(final int start, final int end, final String group) {
            this.start = start;
            this.end = end;
            this.group = group;
        }
    }

    public interface RegexReplacement {
        String replace(String match);
    }

    private RegexSubstitutor() {
    }

    /**
     * Replace <code>maxMatches</code> from the beginning of <code>source</code>
     *
     * @param target      the source
     * @param pattern     the pattern
     * @param replacement the replacement
     * @param maxMatches  how many matches (from source-start) would you like to have replaced
     * @return the replacement
     */
    public static String replaceCount(String target,
                                      Pattern pattern,
                                      String replacement,
                                      int maxMatches) {
        StringBuffer sb = new StringBuffer();
        int matchCount = 0;
        Matcher matcher = pattern.matcher(target);
        boolean result = matcher.find();
        while (result) {
            matcher.appendReplacement(sb, replacement);
            matchCount++;
            if (matchCount == maxMatches) {
                break;
            } else {
                result = matcher.find();
            }
        }
        matcher.appendTail(sb);
        target = sb.toString(); // Allows replacement chaining.
        return (target);
    }

    /**
     * Replace match with return from given command
     *
     * @param target      the source
     * @param pattern     the pattern
     * @param replacement the replacement
     * @return the replacement
     */
    public static String replaceCommand(String target,
                                        Pattern pattern,
                                        RegexReplacement replacement) {
        StringBuffer sb = new StringBuffer();
        Matcher matcher = pattern.matcher(target);
        boolean result = matcher.find();
        while (result) {
            String replaceStr = replacement.replace(matcher.group());
            matcher.appendReplacement(sb, replaceStr);
            result = matcher.find();
        }
        matcher.appendTail(sb);
        target = sb.toString(); // Allows replacement chaining.
        return (target);
    }

    /**
     * Replace <code>maxMatches</code> from the end of <code>source</code>
     *
     * @param target      the source
     * @param pattern     the pattern
     * @param replacement the replacement. Can contain $1 for replacement of the matched pattern.
     * @param maxMatches  how many matches (from source-end) would you like to have replaced
     * @return the replacement
     */
    public static String replaceLastCount(String target,
                                          Pattern pattern,
                                          String replacement,
                                          int maxMatches) {
        StringBuilder sb = new StringBuilder();
        int matchCount = 0;
        Matcher matcher = pattern.matcher(target);
        ArrayList<MatchInfo> list = new ArrayList<MatchInfo>();
        // Count the matches.
        boolean result = matcher.find();
        while (result) {
            list.add(new MatchInfo(matcher.start(),
                    matcher.end(),
                    matcher.group()));
            matchCount++;
            result = matcher.find();
        }
        // If there were no matches, we are done
        if (matchCount == 0) {
            return target;
        }
        // Calculate the loop offset where replacements begin.
        int replaceLoopOffset = maxMatches > matchCount ? 0 : matchCount - maxMatches;
        // Loop over the matches and either append them or replace them.
        int currentIndex = 0;
        for (int loopIndex = 0; loopIndex < matchCount; loopIndex++) {
            MatchInfo matchInfo = list.get(loopIndex);
            int startIndex = matchInfo.start;
            int endIndex = matchInfo.end;
            // If necessary, copy the chars before the match.
            if (startIndex > currentIndex) {
                sb.append(target, currentIndex, startIndex);
            }
            currentIndex = startIndex;
            // If we are before the replacement point, copy the chars.
            if (loopIndex < replaceLoopOffset) {
                sb.append(matchInfo.group);
            } else {
                String r = replacement.replaceAll("\\$1", matchInfo.group);
                sb.append(r);
            }
            currentIndex = endIndex;
        }
        // If necessary, copy the remaining characters.
        if (currentIndex <= target.length()) {
            sb.append(target.substring(currentIndex));
        }
        target = sb.toString(); // Allows replacement chaining.
        return (target);
    }

    public static String format(String pattern, final Object... values) {
        String ret = replaceCommand(pattern, Pattern.compile("\\([^)]*\\)"), new RegexReplacement() {
            int matchCount = -1;

            @Override
            public String replace(String match) {
                // strip away the bracket
                match = match.substring(1, match.length() - 1);

                // convert regex to format string
                // TODO: we need to create a more sophisticated method instead of this hard-coded single-case approach
                match = match.replace("[0-9][0-9]", "%02d");

                matchCount++;
                Object value = values[matchCount];

                return String.format(match, value);
            }
        });

        return ret;
    }
}
