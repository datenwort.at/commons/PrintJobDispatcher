/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.dm;

import at.datenwort.printJobDispatcher.dblib.DocumentInterface;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QueuedDocumentManagementDelivery implements DocumentManagementDelivery {
    private final DocumentManagementDelivery delegate;

    private final List<Infos> queuedDeliveries = new ArrayList<>();
    private final Map<File, Infos> pathMap = new HashMap<>();

    private static class Infos {
        private final File pdfFile;
        private final DocumentInterface doc;

        private Infos(File pdfFile, DocumentInterface doc) {
            this.pdfFile = pdfFile;
            this.doc = doc;
        }
    }

    public QueuedDocumentManagementDelivery(DocumentManagementDelivery delegate) {
        this.delegate = delegate;
    }

    @Override
    public void deliverDocument(File pdfFile, DocumentInterface doc) {
        Infos infos = new Infos(pdfFile, doc);
        queuedDeliveries.add(infos);
        pathMap.put(pdfFile, infos);
    }

    public void deliver() throws IOException {
        for (Infos info : queuedDeliveries) {
            delegate.deliverDocument(info.pdfFile, info.doc);
        }
    }

    public void delivered(File path) {
        Infos info = pathMap.get(path);
        if (info != null) {
            queuedDeliveries.remove(info);
        }
    }
}
