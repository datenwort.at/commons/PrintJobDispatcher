/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.server;

import at.datenwort.printJobDispatcher.config.PjdConfig;
import at.datenwort.printJobDispatcher.lib.FileUtils;
import at.datenwort.printJobDispatcher.lib.Utils;
import at.datenwort.printJobDispatcher.server.processes.PrintJobDispatch;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.attribute.standard.Media;
import javax.print.attribute.standard.PrinterName;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * awaits requests via network - after startup it scans for not jet processed printjobs
 */
@Service
public class NetworkServer {
    private final static Log log = LogFactory.getLog(NetworkServer.class);

    // private String listen = null;

    private ServerSocket socket = null;

    private final JobQueue jobQueue = new JobQueue();
    private ProcessorServer processorServer;

    protected void _run() throws IOException {
        socket = new ServerSocket();
        String listen = PjdConfig.getInstance().getNetworkBind();
        int pos = listen.indexOf(":");
        if (pos < 0) {
            throw new IllegalArgumentException("Listen address invalid: " + listen + " - usage: host:port");
        }

        String host = listen.substring(0, pos);
        int port = Integer.parseInt(listen.substring(pos + 1));

        log.info(getClass().getName() + " startup. Listen on " + host + ":" + port);

        socket.bind(new InetSocketAddress(host, port));

        scanDirectory();

        processorServer = new ProcessorServer(jobQueue);
        processorServer.init();
        processorServer.start();

        while (!Thread.interrupted() && socket.isBound() && !socket.isClosed()) {
            Socket client = socket.accept();

            Thread t = new Thread(new ClientRequestService(this, client));
            t.setDaemon(true);
            t.start();
        }

        shutdown();
    }

    private void shutdown() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                log.warn(e.getLocalizedMessage(), e);
            }
        }

        if (processorServer != null) {
            processorServer.interrupt();
        }
    }

    private void scanDirectory() {
        try {
            for (String basePath : Utils.split(';', PjdConfig.getInstance().getSpoolPath())) {
                File basePathFile = new File(basePath);

                File[] jobs = basePathFile.listFiles(FileUtils.getDirectoryFileFilter());
                if (jobs == null || jobs.length < 1) {
                    return;
                }

                for (File job : jobs) {
                    File data1 = new File(job, "data.pdf");
                    File data2 = new File(job, "data.txt");
                    File data3 = new File(job, "data.bin");
                    File done = new File(job, "done");

                    if ((data1.exists() || data2.exists() || data3.exists()) && !done.exists()) {
                        jobQueue.addProcess(new PrintJobDispatch(job));
                    }
                }
            }
        } catch (Exception e) {
            log.fatal(e.getLocalizedMessage(), e);
        }
    }

    public void stopApplication() {
        shutdown();
    }

    public JobQueue getJobQueue() {
        return jobQueue;
    }

    public static void main(String[] args) throws Exception {
        if (args != null && args.length > 0) {
            if ("lp".equalsIgnoreCase(args[0])) {
                PrintService[] pss = javax.print.PrintServiceLookup.lookupPrintServices(DocFlavor.SERVICE_FORMATTED.PAGEABLE, null);
                for (PrintService ps : pss) {
                    PrinterName printerName = ps.getAttribute(PrinterName.class);
                    System.err.println("Printer: '" + ps.getName() + "' " + Utils.join(",", ps.getSupportedDocFlavors()));
                    System.err.println("Name:" + printerName.toString());
                    System.err.println("\tTrays:");
                    Media[] medias = (Media[]) ps.getSupportedAttributeValues(Media.class, null, null);
                    for (Media media : medias) {
                        System.err.println("\t'" + media.toString() + "'");
                    }
                }

                return;
            }
        }
        NetworkServer ns = new NetworkServer();

        ns._run();
    }
}
