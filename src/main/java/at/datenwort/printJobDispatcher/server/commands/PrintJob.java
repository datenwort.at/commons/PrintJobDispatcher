/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.server.commands;

import at.datenwort.printJobDispatcher.config.PjdConfig;
import at.datenwort.printJobDispatcher.server.NetworkServer;
import at.datenwort.printJobDispatcher.server.processes.PrintJobDispatch;

import java.io.File;

public class PrintJob implements Command {
    public boolean execute(final NetworkServer networkServer, final String args) {
        networkServer.getJobQueue().addProcess(new PrintJobDispatch(new File(PjdConfig.getInstance().getSpoolPath(), args)));
        return true;
    }
}
