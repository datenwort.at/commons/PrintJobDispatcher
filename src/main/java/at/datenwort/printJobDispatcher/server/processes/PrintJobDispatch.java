/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.server.processes;

import at.datenwort.printJobDispatcher.config.ConfigurationUtils;
import at.datenwort.printJobDispatcher.config.DocumentAction;
import at.datenwort.printJobDispatcher.config.OverlayConfig;
import at.datenwort.printJobDispatcher.config.PDFOverlayConfiguration;
import at.datenwort.printJobDispatcher.config.PjdConfig;
import at.datenwort.printJobDispatcher.config.PrintJobset;
import at.datenwort.printJobDispatcher.dblib.DocumentInterface;
import at.datenwort.printJobDispatcher.dm.CompositeDocumentManagementDelivery;
import at.datenwort.printJobDispatcher.dm.DocumentManagementDelivery;
import at.datenwort.printJobDispatcher.dm.QueuedDocumentManagementDelivery;
import at.datenwort.printJobDispatcher.lib.DocumentActionInterface;
import at.datenwort.printJobDispatcher.lib.FileUtils;
import at.datenwort.printJobDispatcher.lib.Utils;
import at.datenwort.printJobDispatcher.lib.printer.JavaPrinterDevice;
import at.datenwort.printJobDispatcher.lib.printer.PrinterDevice;
import at.datenwort.printJobDispatcher.lib.printer.PrinterEnvironment;
import at.datenwort.printJobDispatcher.pdf.InfoFilePDFSplit;
import at.datenwort.printJobDispatcher.pdf.PDFOverlayer;
import at.datenwort.printJobDispatcher.pdf.PDFPrint;
import at.datenwort.printJobDispatcher.server.PrintJobSetup;
import at.datenwort.printJobDispatcher.server.PrintJobSetupFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.JobName;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Verarbeitet einen Druck-Job. Foldende Dateien wird in einem Verzeichnis:
 * <ul>
 * <li>Die Druckdaten in der Datei <code>data.pdf</code></li>
 * <li>Ein Konfigurations-Datei <code>config</code>. Folgende Paramter sind mögich:
 * <ul>
 * <li>printer<br />Druckername für Ausgabe</li>
 * <li>ftitle<br />Titel des Druckjobs</li>
 * <li>fuser<br />Benutzer des Druckjobs</li>
 * <li>fcopies<br />Anzahl Kopien</li>
 * <li>passThrough<br />Anzahl Kopien</li>
 * </ul>
 * </li>
 * </ul>
 */
public class PrintJobDispatch implements Process, ProcessLogger {
    private final static Pattern scriptPattern = Pattern.compile("\\$\\{[^\\}]+\\}+\\$", Pattern.MULTILINE);
    private final static String OVERLAY_EXT = "_OVERLAY";

    private final File jobDir;

    public PrintJobDispatch(File jobDir) {
        this.jobDir = jobDir;
    }

    @Override
    public int hashCode() {
        return jobDir.toString().hashCode();
    }

    public void process() throws Exception {
        log("PrintJob: " + jobDir.getAbsolutePath(), null);
        if (!jobDir.exists() || !jobDir.isDirectory()) {
            throw new IllegalArgumentException("job '" + jobDir.getAbsolutePath() + "' not existent or not a directory");
        }

        PrintQueue printQueue = new PrintQueue();

        PrintJobSetup config = PrintJobSetupFactory.create(jobDir);
        if (config.is(PrintJobSetup.KEY_PASS_THROUGH)) {
            doPassthrough(printQueue, config);
        } else {
            File dataPdf = new File(jobDir, "data.pdf");
            doPjdProcessing(printQueue, dataPdf, config);
        }

        PDFPrint pdfPrint = PDFPrint.Accessor.get();
        for (PrintQueue.Entry printJob : printQueue) {
            log("printing %s to %s", null, printJob.getDocument().getAbsolutePath(), printJob.getPrinter());

            try {
                String documentName = printJob.getDocument().getName().toLowerCase();
                if (documentName.endsWith(".pdf")) {
                    // print using pdf printer
                    pdfPrint.print(printJob.getPrinter(), printJob.getDocument(), printJob.getTitle(), printJob.getCopies(), printJob.getTray(), false);
                } else if (documentName.endsWith(".txt")) {
                    // print using our printer environment to correctly deal with encoding
                    String printer = printJob.getPrinter();
                    final PrinterDevice pd = PrinterEnvironment.getPrinterEnvironment().findDevice(printer);
                    if (pd == null) {
                        throw new IllegalArgumentException("printer " + printer + " not found");
                    }

                    Reader reader = null;
                    try {
                        reader = new InputStreamReader(new FileInputStream(printJob.getDocument()), "UTF-8");
                        Writer writer = pd.openText();
                        FileUtils.copy(reader, writer);
                        pd.close();
                    } finally {
                        if (reader != null) {
                            reader.close();
                            reader = null;
                        }
                    }
                } else if (documentName.endsWith(".bin")) {
                    // directly use javas printerservice
                    String printer = printJob.getPrinter();
                    final PrinterDevice pd = PrinterEnvironment.getPrinterEnvironment().findDevice(printer);
                    if (pd == null) {
                        throw new IllegalArgumentException("printer " + printer + " not found");
                    }
                    spoolFile(printJob.getDocument(), ((JavaPrinterDevice) pd).getPrintService());
                }
            } catch (Exception e) {
                log("problems printing %s", e, printJob.getDocument().getAbsolutePath());
            }
        }

        markAsDone(0 /*ret*/);
    }

    public void spoolFile(File file, PrintService printService) throws IOException, PrintException {
        DocPrintJob job = printService.createPrintJob();

        DocAttributeSet das = new HashDocAttributeSet();

        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        pras.add(new JobName(file.getName(), null));

        Doc doc = new SimpleDoc(new FileInputStream(file), DocFlavor.INPUT_STREAM.AUTOSENSE, das);
        job.print(doc, pras);
    }

    private void doPassthrough(PrintQueue printQueue, PrintJobSetup config) {
        String printer = Utils.getEnvDef("pj_forcePrinter", config.get(PrintJobSetup.KEY_PRINTER));
        if (printer != null && printer.toUpperCase().endsWith(OVERLAY_EXT)) {
            // strip off the overlay extension to avoid any further overlay processing in an loop-back print processing
            // in fact, this avoids the loop-back processing at all
            printer = printer.substring(0, printer.length() - OVERLAY_EXT.length());
        }

        File dataFile = new File(jobDir, "data.pdf");
        if (!dataFile.exists()) {
            dataFile = new File(jobDir, "data.bin");
        }
        if (!dataFile.exists()) {
            dataFile = new File(jobDir, "data.txt");
        }
        if (!dataFile.exists()) {
            throw new IllegalArgumentException("Es konnte kein Druckfile in " + jobDir + " gefunden werden.");
        }

        writeJobEntry(
                printQueue,
                printer,
                Integer.valueOf(def(config.get(PrintJobSetup.KEY_COPIES), "1"), 10),
                dataFile,
                config.get(PrintJobSetup.KEY_TITLE),
                config.get(PrintJobSetup.KEY_USER),
                null,
                false);
    }

    private void doPjdProcessing(PrintQueue printQueue, File dataPdf, PrintJobSetup config) throws Exception {
        List<DocumentInterface> collected = new ArrayList<DocumentInterface>(100);

        InfoFilePDFSplit splitter = new InfoFilePDFSplit();
        splitter.setCollectInto(collected);
        splitter.setPDFOutDir(jobDir.getAbsolutePath());

        QueuedDocumentManagementDelivery queuedDmDelivery = null;
        DocumentManagementDelivery dmDelivery = null;

        String[] dmDeliveries = PjdConfig.getInstance().getDmDeliveries();
        if (dmDeliveries != null) {
            List<DocumentManagementDelivery> dmd = new ArrayList<DocumentManagementDelivery>();
            for (String dmDeliveryClass : dmDeliveries) {
                if (Utils.isEmpty(dmDeliveryClass)) {
                    continue;
                }

                dmd.add((DocumentManagementDelivery) Class.forName(dmDeliveryClass).newInstance());
            }

            if (dmd.size() > 0) {
                dmDelivery = new CompositeDocumentManagementDelivery(
                        dmd.toArray(new DocumentManagementDelivery[dmd.size()]));
            }
        }

        if (dmDelivery != null) {
            queuedDmDelivery = new QueuedDocumentManagementDelivery(dmDelivery);
            splitter.setDocumentManagementDelivery(queuedDmDelivery);
        }

        splitter.splitDocument(dataPdf, jobDir.getAbsolutePath() + "/split_{0}.pdf");

        config.put(PrintJobSetup.KEY_PRINTER_ORI, config.get(PrintJobSetup.KEY_PRINTER));
        config.put(PrintJobSetup.KEY_PRINTER, pjdToRealQueue(config.get(PrintJobSetup.KEY_PRINTER)));

        String printerAnhaenge = config.get(PrintJobSetup.KEY_PRINTER);
        String printerAnhaengeTray = null;

        for (DocumentInterface doc : collected) {
            log("Splitted: " + doc.getPath(), null);

            PjdConfig configuration = PjdConfig.getInstance();

            // read pdf
            ByteArrayOutputStream pdfBuf = readPDF(doc);

            // determine patterns
            Set<String> patterns = ConfigurationUtils.findPatternIds(configuration, doc);

            // the default overlay
            overlayDefaultType(patterns, configuration, doc, pdfBuf, printerAnhaenge);

            // determine the print queues
            PrintJobset printJobset = determinePrintQueues(doc, configuration, patterns);

            // overlay special printer overlay and create job
            byte[] pdfData = pdfBuf.toByteArray();
            pdfBuf.reset();


            int jobno = 0;
            File baseDocument = writePDFData(pdfData, jobno, doc.getPath());

            if (printJobset != null && printJobset.getJobDispatches() != null && printJobset.getJobDispatches().size() > 0) {
                Iterator<at.datenwort.printJobDispatcher.config.PrintJobDispatch> iterJobDispatches = printJobset.getJobDispatches().iterator();

                byte[] pdfDataSave = null;

                while (iterJobDispatches.hasNext()) {
                    jobno++;

                    at.datenwort.printJobDispatcher.config.PrintJobDispatch dispatchConfig = iterJobDispatches.next();

                    log("dispatch: " + dispatchConfig.getQueue() + " tray " + dispatchConfig.getTray() + " overlay " + dispatchConfig.getOverlay(), null);

                    if (dispatchConfig.isPrintAttachments()) {
                        printerAnhaenge = String.valueOf(eval(config, doc, dispatchConfig.getQueue()));
                        printerAnhaengeTray = dispatchConfig.getTray();
                    }

                    if (pdfDataSave != null) {
                        pdfData = pdfDataSave;
                    }

                    if (dispatchConfig.getOverlay() != null) {
                        pdfDataSave = new byte[pdfData.length];
                        System.arraycopy(pdfData, 0, pdfDataSave, 0, pdfData.length);

                        String[] overlays = Utils.splitString(dispatchConfig.getOverlay());
                        for (String overlay : overlays) {
                            overlayPrinterType(configuration, overlay, pdfBuf, pdfData);

                            pdfData = pdfBuf.toByteArray();
                            pdfBuf.reset();
                        }
                    }

                    Integer copies = calculateCopies(config, doc, dispatchConfig);

                    log("to " + dispatchConfig.getQueue() + " with overlay " + dispatchConfig.getOverlay() + " times " + copies, null);

                    File outputPath = writePDFData(pdfData, jobno, doc.getPath());
                    String printer = String.valueOf(eval(config, doc, dispatchConfig.getQueue()));

                    writeJobEntry(
                            printQueue,
                            printer,
                            copies,
                            outputPath,
                            config.get(PrintJobSetup.KEY_TITLE),
                            config.get(PrintJobSetup.KEY_USER),
                            dispatchConfig.getTray(),
                            dispatchConfig.isMonochrome());

                    if (dmDelivery != null && dispatchConfig.isDmCopy()) {
                        // copy to use for document management
                        dmDelivery.deliverDocument(outputPath, doc);
                        queuedDmDelivery.delivered(doc.getPath());
                    }
                }
                if (pdfDataSave != null) {
                    pdfData = pdfDataSave;
                }

                processDocumentActions(doc, baseDocument, printerAnhaenge);
            } else {
                // no jobsetup found - simply pass through
                String printer = config.get(PrintJobSetup.KEY_PRINTER);

                writeJobEntry(
                        printQueue,
                        printer,
                        Integer.valueOf(def(config.get(PrintJobSetup.KEY_COPIES), "1"), 10),
                        baseDocument,
                        config.get(PrintJobSetup.KEY_TITLE),
                        config.get(PrintJobSetup.KEY_USER),
                        null,
                        false);

                processDocumentActions(doc, baseDocument, printerAnhaenge);
            }

            String[] anhaenge = doc.getCategoryValues("Anhang");
            if (anhaenge != null && anhaenge.length > 0) {
                for (String anAnhaenge : anhaenge) {
                    String outputPathAnhang = anAnhaenge; /*SystemConfiguration.getInstance().getPathMaps().getConvertedPath(anAnhaenge);*/

                    writeJobEntry(
                            printQueue,
                            printerAnhaenge,
                            Integer.valueOf(1),
                            new File(outputPathAnhang),
                            config.get(PrintJobSetup.KEY_TITLE),
                            config.get(PrintJobSetup.KEY_USER),
                            printerAnhaengeTray,
                            false);
                }
            }
        }

        if (queuedDmDelivery != null) {
            queuedDmDelivery.deliver();
        }

        // create job-list
        // writeJoblist(printQueue);
    }

    /**
     * calculate the copies by assuming that the warehouse system means 1 original + n copies. So, 0 copies means "print 1 times".
     */
    private Integer calculateCopies(PrintJobSetup config, DocumentInterface doc, at.datenwort.printJobDispatcher.config.PrintJobDispatch dispatchConfig)
            throws ScriptException {
        int copies = Integer.parseInt(def(eval(config, doc, dispatchConfig.getCopies()), "1"), 10);
        if (copies < 1) {
            return 1;
        }

        return copies;
    }

    protected String pjdToRealQueue(String pjdPrinter) {
        if (pjdPrinter != null && pjdPrinter.endsWith(OVERLAY_EXT)) {
            return pjdPrinter.substring(0, pjdPrinter.length() - OVERLAY_EXT.length());
        }

        return pjdPrinter;
    }

    private void processDocumentActions(final DocumentInterface doc, final File pdfFile, String printer) throws Exception, IllegalAccessException, InstantiationException {
        PjdConfig conf = PjdConfig.getInstance();
        Set patterns = ConfigurationUtils.findPatternIds(conf, doc);

        List<DocumentAction> actions = ConfigurationUtils.getDocumentActions(doc.getMandant(), conf, patterns, printer);
        if (actions == null || actions.size() < 1) {
            log("no document actions to process", null);
            return;
        }

        for (DocumentAction action : actions) {
            log("processing document action %s", null, action.getClassName());

            final DocumentActionInterface dai = action.getDocumentAction();
            if (dai == null) {
                continue;
            }

            // synchronized(action.getPluginMutex())
            {
                dai.process(this, doc, pdfFile);
            }

            /*
               PluginManager.getInstance().call(dai.getClass(), new Callable<Void>()
               {
                   @Override
                   public Void call() throws Exception
                   {
                       dai.process(doc, pdfFile);
                       return null;
                   }
               });
               */
        }
    }

    private void writeJobEntry(PrintQueue printQueue, String printer, Integer copies, File document, String title, String user, String tray, boolean monochrome) {
        log(String.format("prepare for printing %s (%s) to %s copies %d (Tray: %s, Monochrome: %s)", title, document.getAbsolutePath(), printer, copies, tray, monochrome ? "ja" : "nein"), null);
        printQueue.add(
                Utils.getEnvDef("pj_forcePrinter", printer),
                copies, document, title, user, tray, monochrome);
    }

    public void log(String log, Throwable exception, Object... args) {
        File joblist = new File(jobDir, "log");
        PrintWriter osw = null;
        try {
            osw = new PrintWriter(new FileWriter(joblist, true));
            osw.println(String.format(log, args));
            if (exception != null) {
                exception.printStackTrace(osw);
            }
        } catch (IOException e) {
            Log _log = LogFactory.getLog(PrintJobDispatch.class);
            _log.fatal("Error writing log", e);
            _log.info(log, exception);
        } finally {
            if (osw != null) {
                osw.close();
            }
        }
    }

    private void overlayPrinterType(PjdConfig configuration, String overlay, ByteArrayOutputStream pdfBuf, byte[] pdfData) throws IOException {
        OverlayConfig overlayConfig = configuration.getOverlayConfig(overlay);
        if (overlayConfig == null) {
            log("overlayConfig for overlay " + overlay + " not found.", null);
            return;
        }

        PDFOverlayConfiguration pdfOverlayConfig = overlayConfig.getOverlay("pdf");

        log("overlay: " + pdfOverlayConfig.getId(), null);

        PDFOverlayer overlayer = new PDFOverlayer(pdfOverlayConfig);
        overlayer.setOutputStream(pdfBuf);
        overlayer.setInput(pdfData);
        pdfBuf.reset();

        overlayer.process();
    }

    private PrintJobset determinePrintQueues(DocumentInterface doc, PjdConfig configuration, Set patterns) {
        PrintJobset printJobset = ConfigurationUtils.getPrintJobset(
                doc.getMandant(),
                configuration,
                Utils.getFirstOrNull(doc.getCategoryValues("Kopien")),
                Utils.getFirstOrNull(doc.getCategoryValues("Abteilung")),
                patterns);
        return printJobset;
    }

    private void overlayDefaultType(Set<String> patterns, PjdConfig configuration, DocumentInterface doc, ByteArrayOutputStream pdfBuf, String printer) throws IOException {
        PDFOverlayConfiguration overlay;
        if (patterns.size() > 0) {
            overlay = ConfigurationUtils.getOverlayConfig(
                    doc.getMandant(),
                    "printPDF",
                    Utils.getFirstOrNull(doc.getCategoryValues("Abteilung")),
                    Utils.getFirstOrNull(doc.getCategoryValues("Layout")),
                    configuration,
                    patterns,
                    "pdf",
                    printer);

            if (overlay != null) {
                PDFOverlayer overlayer = new PDFOverlayer(overlay);
                overlayer.setInput(pdfBuf.toByteArray());

                pdfBuf.reset();
                overlayer.setOutputStream(pdfBuf);

                overlayer.process();
            }
        }
    }

    private ByteArrayOutputStream readPDF(DocumentInterface doc)
            throws IOException {
        ByteArrayOutputStream pdfBuf = new ByteArrayOutputStream(1024000);
        FileUtils.copyTo(doc.getPath(), pdfBuf);
        pdfBuf.close();

        return pdfBuf;
    }

    private File writePDFData(byte[] pdfData, int jobno, File docname)
            throws IOException {
        String jobDocname = docname.getName().replaceAll("\\W", "") + "_" + jobno + "_.pdf";
        File output = new File(docname.getParentFile(), jobDocname);

        OutputStream os = null;
        try {
            os = new FileOutputStream(output);
            os.write(pdfData);
        } finally {
            if (os != null) {
                os.close();
            }
        }
        return output;
    }

    private String def(String o, String s) {
        if (o == null || o.length() < 1) {
            return s;
        }

        return o;
    }

    private String eval(PrintJobSetup config, DocumentInterface doc, String text) throws ScriptException {
        log("evaluate " + text, null);

        if (text == null) {
            return null;
        }

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");

        engine.put("job", config);
        engine.put("doc", doc);

        Matcher matcher = scriptPattern.matcher(text);
        StringBuilder sb = new StringBuilder(text.length());
        while (matcher.find()) {
            String script = matcher.group();
            log("found scriptlet: " + script, null);

            script = script.substring(2, script.length() - 2);

            Object value = engine.eval(script);
            log("returns: " + script, null);

            if (value != null) {
                matcher.appendReplacement(sb, String.valueOf(value));
            } else {
                matcher.appendReplacement(sb, "");
            }
        }
        matcher.appendTail(sb);

        log("result: " + sb, null);

        return sb.toString();
    }

    private void markAsDone(int ret) throws IOException {
        File done = new File(jobDir, "done");
        try (Writer writer = new FileWriter(done)) {
            writer.write("Printjob-list process done with: " + ret);
        }
    }
}
