/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.server.processes;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PrintQueue implements Iterable<PrintQueue.Entry> {
    public static class Entry {
        private final String printer;
        private final Integer copies;
        private final File document;
        private final String title;
        private final String user;
        private final String tray;
        private final boolean monocrome;

        public Entry(String printer, Integer copies, File document, String title, String user, String tray, boolean monocrome) {
            this.printer = printer;
            this.copies = copies;
            this.document = document;
            this.title = title;
            this.user = user;
            this.tray = tray;
            this.monocrome = monocrome;
        }

        public String getTray() {
            return tray;
        }

        public String getPrinter() {
            return printer;
        }

        public Integer getCopies() {
            return copies;
        }

        public File getDocument() {
            return document;
        }

        public String getTitle() {
            return title;
        }

        public String getUser() {
            return user;
        }

        public boolean isMonocrome() {
            return monocrome;
        }
    }

    private final List<Entry> queue = new ArrayList<Entry>();

    public Iterator<Entry> iterator() {
        return queue.iterator();
    }

    public void add(String printer, Integer copies, File document, String title, String user, String tray, boolean monochrome) {
        queue.add(new Entry(printer, copies, document, title, user, tray, monochrome));
    }
}
