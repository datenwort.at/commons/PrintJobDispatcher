/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.server;

import at.datenwort.printJobDispatcher.server.processes.Process;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

public class ProcessorServer extends Thread {
    private final static Log log = LogFactory.getLog(ProcessorServer.class);

    private final JobQueue jobQueue;

    private GenericObjectPool<Worker> pool;

    public static class Worker extends Thread {
        private final static Log log = LogFactory.getLog(Worker.class);

        private final GenericObjectPool<Worker> pool;

        private Process jobLatch;
        private final Object newJob = new Object();

        private volatile boolean exitLoop = false;

        public Worker(GenericObjectPool<Worker> pool) {
            setDaemon(true);
            this.pool = pool;
        }

        public void run() {
            while (!exitLoop && !Thread.currentThread().isInterrupted()) {
                synchronized (newJob) {
                    while (!exitLoop && !Thread.currentThread().isInterrupted() && jobLatch == null) {
                        try {
                            newJob.wait(100);
                        } catch (InterruptedException e) {
                            if (log.isDebugEnabled()) {
                                log.debug("worker " + this + " wait interrupted");
                            }

                            returnWorker();
                            return;
                        }
                    }
                }

                if (log.isDebugEnabled()) {
                    log.debug("worker " + this + " got job " + jobLatch);
                }

                if (jobLatch != null) {
                    try {
                        jobLatch.process();
                    } catch (Exception e) {
                        log.fatal(e.getLocalizedMessage(), e);
                    } finally {
                        jobLatch = null;

                        if (log.isDebugEnabled()) {
                            log.debug("return worker " + this);
                        }

                        if (!returnWorker()) {
                            // hopefully we get out here
                            Thread.currentThread().interrupt();
                        }
                    }
                }
            }

            if (log.isDebugEnabled()) {
                log.debug("worker " + this + " ended");
            }
        }

        private boolean returnWorker() {
            boolean ok = true;
            try {
                pool.returnObject(this);
            } catch (Exception e) {
                ok = false;
                log.fatal(e.getLocalizedMessage(), e);
            }

            return ok;
        }

        public void processJob(Process job) {
            jobLatch = job;

            synchronized (newJob) {
                newJob.notifyAll();
            }
        }
    }

    public static class WorkerFactory implements PoolableObjectFactory<Worker> {
        private final static Log log = LogFactory.getLog(WorkerFactory.class);

        private GenericObjectPool<Worker> pool;

        public WorkerFactory() {
        }

        public void setPool(GenericObjectPool<Worker> pool) {
            this.pool = pool;
        }

        public Worker makeObject() {
            Worker worker = new Worker(pool);

            if (log.isDebugEnabled()) {
                log.debug("created new worker " + worker);
            }

            worker.start();
            return worker;
        }

        public void destroyObject(Worker worker) throws Exception {
            if (log.isDebugEnabled()) {
                log.debug("destroy worker " + worker);
            }

            int nuof = 10;
            worker.exitLoop = true;
            while (nuof > 0 && worker.isAlive()) {
                worker.interrupt();

                Thread.sleep(100);

                nuof--;
            }
        }

        public boolean validateObject(Worker worker) {
            return worker.isAlive();
        }

        public void activateObject(Worker o) {
        }

        public void passivateObject(Worker o) {
        }
    }

    public ProcessorServer(JobQueue jobQueue) {
        this.jobQueue = jobQueue;
    }

    public void init() {
        WorkerFactory workerFactory = new WorkerFactory();
        pool = new GenericObjectPool<>(workerFactory);
        workerFactory.setPool(pool);
        pool.setMaxActive(10);
        pool.setMaxIdle(10);
        pool.setMinIdle(5);
        pool.setTestOnReturn(true);
        pool.setTestOnBorrow(true);
        pool.setTestWhileIdle(true);
        pool.setTimeBetweenEvictionRunsMillis(5000);
        pool.setMaxWait(60000); // one minute
        pool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_BLOCK);
    }

    public void close() {
        if (pool != null) {
            try {
                pool.close();
            } catch (Exception e) {
                log.fatal(e.getLocalizedMessage(), e);
            }
        }
    }

    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Process job = jobQueue.getNextProcess();
            Worker worker;
            try {
                worker = pool.borrowObject();
            } catch (Exception e) {
                log.fatal(e.getLocalizedMessage(), e);

                jobQueue.addProcess(job);

                continue;
            }

            worker.processJob(job);
        }

        try {
            pool.close();
        } catch (Exception e) {
            log.warn(e.getLocalizedMessage(), e);
        }
    }
}
