/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.server;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PrintJobSetupFactory {
    private final static Log log = LogFactory.getLog(PrintJobSetupFactory.class);

    private PrintJobSetupFactory() {
    }

    public static PrintJobSetup create(File jobdir) throws IOException {
        PrintJobSetup setup = new PrintJobSetup();

        // readFilename(setup, file);
        readKeyValueFile(setup, jobdir, "config");
        readPs(setup, jobdir);
        // readCmdFile(setup, basedir, "cmd");

        return setup;
    }

    private static void readPs(PrintJobSetup setup, File jobdir) throws IOException {
        File dataPs = new File(jobdir, "data.ps");
        if (!dataPs.exists() || !dataPs.canRead()) {
            return;
        }

        try (BufferedReader br = new BufferedReader(new FileReader(dataPs))) {

            int found = 0;

            String line;
            // adjust "found" to the number of configuration options you are searching for
            while (found != 1 && (line = br.readLine()) != null) {
                String arg = parseArg(line, "%%BeginNonPPDFeature", "NumCopies");
                if (arg != null) {
                    found++;
                    try {
                        Integer.parseInt(arg, 10);
                        setup.put(PrintJobSetup.KEY_COPIES, arg);
                    } catch (NumberFormatException e) {
                        // seems a wrong number of copies format
                        log.warn("can't parse number of copies in line '" + line + "' found='" + arg + "'", e);
                    }
                }
            }
        }
    }

    private static String parseArg(String line, String marker, String param) {
        int pos = line.indexOf(marker);
        if (pos < 0) {
            return null;
        }

        pos = line.indexOf(param, pos + marker.length());
        if (pos < 0) {
            return null;
        }

        String paramValue = line.substring(pos + param.length()).trim();
        return paramValue;
    }

    private static void readKeyValueFile(PrintJobSetup setup, File basedir, String name) throws IOException {
        File config = new File(basedir, name);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(config)));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split("=", 2);

                if (!addToken(setup, tokens) && log.isDebugEnabled()) {
                    log.debug(name + ": skipped line: " + line);
                }
            }
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    log.warn(e.getLocalizedMessage(), e);
                }
            }
        }
    }

    private static boolean addToken(PrintJobSetup setup, String[] tokens) {
        switch (tokens.length) {
            case 1 -> {
                setup.put(tokens[0], "true");
                return true;
            }
            case 2 -> {
                setup.put(tokens[0], tokens[1]);
                return true;
            }
        }

        return false;
    }
}
