/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.server;

import at.datenwort.printJobDispatcher.configurationEditor.ConfigurationEditor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Profiles;

@SpringBootApplication(scanBasePackages = "at.datenwort.printJobDispatcher")
public class PrintJobDispatcherApp {
    public static void main(String[] args) {
        SpringApplication.run(PrintJobDispatcherApp.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            if (ctx.getEnvironment().acceptsProfiles(Profiles.of("configuration"))) {
                ConfigurationEditor.main(new String[0]);
            } else {
                NetworkServer networkServer = ctx.getBean(NetworkServer.class);
                networkServer._run();
            }
        };
    }
}
