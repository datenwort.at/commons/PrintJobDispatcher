/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.server;

import at.datenwort.printJobDispatcher.lib.Utils;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class PrintJobSetup
{
	public final static String KEY_COPIES = "fcopies";
	public final static String KEY_JOB = "fjob";
	public final static String KEY_USER = "fuser";
	public final static String KEY_TITLE = "ftitle";
	public final static String KEY_FILENAME = "ffilename";
    public final static String KEY_PRINTER = "printer";
    public final static String KEY_PASS_THROUGH = "passThrough";
	public final static String KEY_PRINTER_ORI = "printer_ori";

	private final Map<String, String> values = new TreeMap<>();

    private final static Map<String, String[]> ALIASES = new HashMap<>();

    static
    {
        ALIASES.put(KEY_PRINTER, Utils.asArray("redmon_printer"));
        ALIASES.put(KEY_TITLE, Utils.asArray("redmon_docname"));
        ALIASES.put(KEY_FILENAME, Utils.asArray("redmon_filename"));
        ALIASES.put(KEY_USER, Utils.asArray("redmon_user"));
        ALIASES.put(KEY_JOB, Utils.asArray("redmon_job"));
    }

	PrintJobSetup()
	{
	}

	public void put(String key, String value)
	{
        String keylc = key.toLowerCase();

		values.put(keylc, value);
	}

	public String get(String key)
	{
        String keylc = key.toLowerCase();

		String ret = values.get(keylc);

        if (ret != null)
        {
            return ret;
        }

        String[] aliases = ALIASES.get(keylc);
        if (aliases != null)
        {
            for (String alias : aliases)
            {
                ret = values.get(alias);
                if (ret != null)
                {
                    return ret;
                }
            }
        }

        return null;
	}

	public boolean is(String key)
	{
		String val = get(key);
		return Utils.guessBool(val);
	}
}
