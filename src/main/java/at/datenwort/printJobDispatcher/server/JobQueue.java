/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.server;

import at.datenwort.printJobDispatcher.server.processes.Process;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class JobQueue {
    private final LinkedHashSet<Process> jobQueue = new LinkedHashSet<>();

    public JobQueue() {
    }

    public void addProcess(Process process) {
        synchronized (jobQueue) {
            if (!jobQueue.contains(process)) {
                jobQueue.add(process);
                jobQueue.notifyAll();
            }
        }
    }

    public Process getNextProcess() {
        synchronized (jobQueue) {
            while (!Thread.currentThread().isInterrupted() && jobQueue.size() < 1) {
                try {
                    jobQueue.wait(100);
                } catch (InterruptedException ignored) {
                    ;
                }
            }

            Iterator<Process> iterQueue = jobQueue.iterator();
            Process process = iterQueue.next();
            iterQueue.remove();

            return process;
        }
    }
}
