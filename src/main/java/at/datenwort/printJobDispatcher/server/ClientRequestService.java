/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.server;

import at.datenwort.printJobDispatcher.server.commands.Command;
import at.datenwort.printJobDispatcher.server.commands.PrintJob;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.util.Map;
import java.util.TreeMap;

public class ClientRequestService implements Runnable {
    private final static Log log = LogFactory.getLog(ClientRequestService.class);

    private final Socket client;
    private final NetworkServer networkServer;

    private Writer output;

    private static final Map<String, Command> COMMANDS = new TreeMap<>();

    static {
        COMMANDS.put("job", new PrintJob());
    }

    public ClientRequestService(NetworkServer networkServer, Socket client) {
        this.networkServer = networkServer;
        this.client = client;
    }

    public void run() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));

            String line;
            while ((line = br.readLine()) != null) {
                if (line.length() > 0) {
                    if (line.equalsIgnoreCase("quit")) {
                        break;
                    }

                    processRequest(line);
                }
                break;
            }

            client.shutdownInput();
            client.shutdownOutput();
            client.close();
        } catch (IOException e) {
            log.fatal(e.getLocalizedMessage(), e);
            sendFatal(e.getLocalizedMessage());
        }
    }

    private void processRequest(String line) {
        String[] tokens = line.split(" ", 2);
        if (tokens.length != 2) {
            sendFatal("invalid request");
            return;
        }

        Command command = COMMANDS.get(tokens[0].toLowerCase());
        if (command == null) {
            sendFatal("unknown command: '" + tokens[0] + "'");
            return;
        }

        boolean ret = command.execute(networkServer, tokens[1]);
        if (ret) {
            sendOk();
        } else {
            sendError();
        }
    }

    private void sendFatal(String message) {
        sendMessage("-ERR " + message);
    }

    private void sendError() {
        sendMessage("-ERR");
    }

    private void sendOk() {
        sendMessage("+OK");
    }

    private void sendMessage(String message) {
        try {
            if (output == null) {
                output = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
            }

            output.write(message);
            output.write('\n');
            output.flush();
        } catch (IOException e) {
            log.warn(e.getLocalizedMessage(), e);
        }
    }
}
