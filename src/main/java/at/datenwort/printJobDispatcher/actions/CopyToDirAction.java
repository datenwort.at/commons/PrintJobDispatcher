/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.actions;

import at.datenwort.printJobDispatcher.dblib.DocumentInterface;
import at.datenwort.printJobDispatcher.lib.DocumentActionInterface;
import at.datenwort.printJobDispatcher.server.processes.ProcessLogger;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.prefs.Preferences;

public class CopyToDirAction implements DocumentActionInterface {
    private final CopyToDirConfig config = new CopyToDirConfig();

    @Override
    public void process(ProcessLogger logger, DocumentInterface doc, File pdfFile) throws Exception {
        String pathPattern = getConfig().getPathPattern();

        String path = String.format(pathPattern,
                new Date(),
                pdfFile.getName(),
                doc.getSimpleName(),
                doc.getExtDocumentNumber());

        Path destination = Paths.get(path);
        Files.createDirectories(destination.getParent());

        Path source = pdfFile.toPath();

        Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING);
    }

    @Override
    public CopyToDirConfig getConfig() {
        return config;
    }

    @Override
    public void initializePlugin(Preferences prefs) {
        getConfig().setPathPattern(prefs.get("pathPattern", null));
    }
}
