/*
 * Print Job Dispatcher
 * Copyright (C) any-time - Mario Ivankovits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package at.datenwort.printJobDispatcher.actions;

public class AbstractEDeliveryConfig {
    private String fixedMailSender;
    private String fixedMailCc;
    private String fixedMailBcc;
    private String mailSenderDomain;
    private String faxSenderDomain;
    private String groupwareServer;
    private String adminRecipient;

    public String getFixedMailSender() {
        return fixedMailSender;
    }

    public void setFixedMailSender(String fixedMailSender) {
        this.fixedMailSender = fixedMailSender;
    }

    public String getFixedMailCc() {
        return fixedMailCc;
    }

    public void setFixedMailCc(String fixedMailCc) {
        this.fixedMailCc = fixedMailCc;
    }

    public String getFixedMailBcc() {
        return fixedMailBcc;
    }

    public void setFixedMailBcc(String fixedMailBcc) {
        this.fixedMailBcc = fixedMailBcc;
    }

    public String getMailSenderDomain() {
        return mailSenderDomain;
    }

    public void setMailSenderDomain(String mailSenderDomain) {
        this.mailSenderDomain = mailSenderDomain;
    }

    public String getFaxSenderDomain() {
        return faxSenderDomain;
    }

    public void setFaxSenderDomain(String faxSenderDomain) {
        this.faxSenderDomain = faxSenderDomain;
    }

    public String getGroupwareServer() {
        return groupwareServer;
    }

    public void setGroupwareServer(String groupwareServer) {
        this.groupwareServer = groupwareServer;
    }

    public String getAdminRecipient() {
        return adminRecipient;
    }

    public void setAdminRecipient(String adminRecipient) {
        this.adminRecipient = adminRecipient;
    }
}
