// pjdfile.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"

TCHAR logFileName[MAX_PATH];
void *minst;

FILE* stdoutStream;
FILE* stderrStream;

static int GSDLLCALL gsdll_stdin(void *instance, char *buf, int len)
{
	fprintf(stderr, "read stdin");
	return -1;
}

static int GSDLLCALL gsdll_stdout(void *instance, const char *str, int len)
{
	fwrite(str, sizeof(char), len, stdoutStream);
	fflush(stdoutStream);
	return len;
}

static int GSDLLCALL gsdll_stderr(void *instance, const char *str, int len) 
{
	fwrite(str, sizeof(char), len, stdoutStream);
	fflush(stderrStream);
	return len;
}

void printError(DWORD error, TCHAR* message, ...)
{
	va_list args;

	TCHAR date[30];
	_wstrdate_s(date);
	TCHAR time[30];
	_wstrtime_s(time);

	TCHAR sysMessage[1024] = _T("\0");

	if (error != 0)
	{
		FormatMessage( 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			error,
			0, // Default language
			sysMessage,
			sizeof(sysMessage),
			NULL);
	}

	TCHAR messageText[1024];

	va_start(args, message);

	StringCbVPrintf(messageText, sizeof(messageText), message, args);

	va_end(args);

	fwprintf(stderr, messageText);


	TCHAR outputText[1024];
	StringCbPrintf(outputText, sizeof(outputText), _T("%s %s: %s (Fehler: %d - %s)"), date, time, messageText, error, sysMessage);

	if (wcslen(logFileName) > 0)
	{
		char utf8[1024];
		WideCharToMultiByte(CP_UTF8, 0, outputText, -1, utf8, sizeof(utf8), NULL, NULL);

		HANDLE logFile = CreateFile(logFileName, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_FLAG_BACKUP_SEMANTICS, NULL);
		if (logFile == INVALID_HANDLE_VALUE)
		{
			wprintf(_T("create data file '%s' failed. Fehler: %d"), logFileName, GetLastError());
		}
		else
		{
			SetFilePointer(logFile, 0, 0, FILE_END);

			DWORD written;
			WriteFile(logFile, utf8, strlen(utf8), &written, NULL);
			WriteFile(logFile, "\r\n", 2, &written, NULL);

			CloseHandle(logFile);

			return;
		}
	}

	// no directory created yet or error
	wprintf(_T("%s\r\n"), outputText);
}


int _tmain(int argc, _TCHAR* argv[], _TCHAR* envp[])
{
	// UTF-8 header
	unsigned char smarker[3];
	smarker[0] = 0xEF;
	smarker[1] = 0xBB;
	smarker[2] = 0xBF;

	// Setup
	TCHAR* baseDir = argv[1]; //_T("c:\\opsj\\spool");

	TCHAR* pjdServer = argv[2]; //_T("localhost:3250");
	TCHAR* pjdPort = wcstok(pjdServer, _T(":"));
	if (pjdPort != NULL)
	{
		pjdPort = wcstok(NULL, _T(":"));
	}


	// setup our global log
	StringCbPrintf(logFileName, sizeof(logFileName), _T("%s\\log"), baseDir);

	// Create unique temporary directory by creating guids until the directory has been created successfully. According to the guid definition this should
	// be on the first try, but I am paranoic.
	// TCHAR tempFileGuid[MAX_PATH];

	int maxTry = 100;
	DWORD createDirErr;

	TCHAR tempDirName[MAX_PATH];
	TCHAR jobName[MAX_PATH];

	int dirnum = 0;
	time_t currentTime;
	time(&currentTime);
	do
	{
		/*
		GUID guid;
		HRESULT createGuidResult = CoCreateGuid(&guid);
		if (createGuidResult != S_OK)
		{
		printError(createGuidResult, _T("Error creating guid"), NULL);
		return 2;
		}

		int guidToStringRet = StringFromGUID2(guid, tempFileGuid, MAX_PATH-1);
		if (guidToStringRet < 1)
		{
		printError(guidToStringRet, _T("Error creating string from guid"), NULL);
		return 2;
		}

		StringCbPrintf(tempDirName, sizeof(tempDirName), _T("%s\\%s"), baseDir, tempFileGuid);
		*/

		StringCbPrintf(jobName, sizeof(jobName), _T("%10d-%06d"), currentTime, dirnum);
		StringCbPrintf(tempDirName, sizeof(tempDirName), _T("%s\\%s"), baseDir, jobName);

		createDirErr = 0;
		maxTry--;
		if (!CreateDirectory(tempDirName, NULL))
		{
			dirnum++;

			createDirErr = GetLastError();

			printError(createDirErr, _T("Error creating directory: '%s'"), tempDirName);
		}
	}
	while (maxTry > 0 && createDirErr == ERROR_ALREADY_EXISTS);

	if (createDirErr != ERROR_SUCCESS)
	{
		// message already printed
		return 2;
	}

	// now setup the system to have a logfile per job
	StringCbPrintf(logFileName, sizeof(logFileName), _T("%s\\log"), tempDirName);


	// write out the current environment to a config file
	TCHAR infoName[MAX_PATH];
	StringCbPrintf(infoName, sizeof(infoName), _T("%s\\config"), tempDirName);

	HANDLE infoFile = CreateFile(infoName, GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_FLAG_BACKUP_SEMANTICS, NULL);
	DWORD written;
	WriteFile(infoFile, smarker, 3, &written, NULL);
	for( int i = 0; envp[i] != NULL; ++i )
	{
		_TCHAR* env = envp[i];

		char utf8[1000];
		WideCharToMultiByte(CP_UTF8, 0, env, -1, utf8, 1000, NULL, NULL);

		// WriteFile(infoFile, env, sizeof(TCHAR)*wcslen(env), &written, NULL);
		WriteFile(infoFile, utf8, strlen(utf8), &written, NULL);
		WriteFile(infoFile, "\r\n", 2, &written, NULL);
	}
	CloseHandle(infoFile);


	// write out the std-in - the actual postscrip data
	TCHAR dataName[MAX_PATH];
	StringCbPrintf(dataName, sizeof(dataName), _T("%s\\data.ps"), tempDirName);

	HANDLE dataFile = CreateFile(dataName, GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_FLAG_BACKUP_SEMANTICS, NULL);
	if (dataFile == INVALID_HANDLE_VALUE)
	{
		printError(GetLastError(), _T("create data file '%s' failed"), dataName);
		return 200;
	}

	size_t len;
	BYTE buf[1024];

	_setmode(_fileno(stdin), O_BINARY);
	while ( (len = fread(buf, 1, sizeof(buf), stdin)) != 0)
	{
		WriteFile(dataFile, buf, len, &written, NULL);
	}
	CloseHandle(dataFile);


	// execute ghostscript to convert the file
	_flushall();

	/* not working system() method
	TCHAR pdfCmd[1000];
	StringCbPrintf(pdfCmd, sizeof(pdfCmd), _T("'c:\\Program Files\\redmon\\ps2pdf.bat'  \"%s\\data.ps\" \"%s\\data.pdf\""), tempDirName, tempDirName);
	printError(0, _T("Executing command: %s"), pdfCmd);

	TCHAR cmdOut[MAX_PATH];
	StringCbPrintf(cmdOut, sizeof(cmdOut), _T("%s\\system.out"), tempDirName);
	TCHAR cmdErr[MAX_PATH];
	StringCbPrintf(cmdErr, sizeof(cmdErr), _T("%s\\system.err"), tempDirName);

	FILE* stdoutStream = _wfreopen(cmdOut, _T("a+"), stdout);
	FILE* stderrStream = _wfreopen(cmdErr, _T("a+"), stderr);
	_wsystem(pdfCmd);
	fclose(stdoutStream);
	fclose(stderrStream);
	*/

	/* Ghostscript DLL */
	TCHAR outputFile[MAX_PATH];
	StringCbPrintf(outputFile, sizeof(outputFile), _T("-sOutputFile=%s\\data.pdf"), tempDirName);

	char outputFileA[MAX_PATH];
	WideCharToMultiByte(CP_ACP, 0, outputFile, -1, outputFileA, sizeof(outputFileA), NULL, NULL);
	char inputFileA[MAX_PATH];
	WideCharToMultiByte(CP_ACP, 0, dataName, -1, inputFileA, sizeof(inputFileA), NULL, NULL);


	gsapi_revision_t r;
	if (gsapi_revision(&r, sizeof(r)) != 0)
	{
		printError(0, _T("Can't read Ghostscript Revision"));

		return 201;
	}

	printError(0, _T("Using Ghostscript Revision %d"), r.revision);

	TCHAR cmdOut[MAX_PATH];
	StringCbPrintf(cmdOut, sizeof(cmdOut), _T("%s\\system.out"), tempDirName);
	TCHAR cmdErr[MAX_PATH];
	StringCbPrintf(cmdErr, sizeof(cmdErr), _T("%s\\system.err"), tempDirName);

	stdoutStream = _wfreopen(cmdOut, _T("a+"), stdout);
	stderrStream = _wfreopen(cmdErr, _T("a+"), stderr);

	char* gsargv[11];
	int gsargc=0;
	gsargv[gsargc++] = "ps2pdf";	/* actual value doesn't matter */
	gsargv[gsargc++] = "-dNOPAUSE";
	gsargv[gsargc++] = "-dBATCH";
	gsargv[gsargc++] = "-dSAFER";
	gsargv[gsargc++] = "-dCompatibilityLevel=1.4";
	gsargv[gsargc++] = "-sDEVICE=pdfwrite";
	gsargv[gsargc++] = outputFileA;
	gsargv[gsargc++] = "-c";
	gsargv[gsargc++] = ".setpdfwrite";
	gsargv[gsargc++] = "-f";
	gsargv[gsargc++] = inputFileA;

	int code = gsapi_new_instance(&minst, NULL);
	if (code < 0)
	{
		printError(0, _T("Failed creating Ghostscript instance %d"), code);
		return 202;
	}

	gsapi_set_stdio(minst, gsdll_stdin, gsdll_stdout, gsdll_stderr);

	code = gsapi_init_with_args(minst, gsargc, gsargv);
	printError(0, _T("After init with args %d"), code);

	gsapi_exit(minst);

	gsapi_delete_instance(minst);

	fclose(stdoutStream);
	fclose(stderrStream);

	if (code != 0)
	{
		return 211;
	}

	// conversion good, create the "pdfok" flag file
	TCHAR pdfOk[MAX_PATH];
	StringCbPrintf(pdfOk, sizeof(pdfOk), _T("%s\\pdfok"), tempDirName);
	HANDLE pdfOkFile = CreateFile(pdfOk, GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_FLAG_BACKUP_SEMANTICS, NULL);
	if (pdfOkFile == INVALID_HANDLE_VALUE)
	{
		printError(GetLastError(), _T("create flag file '%s' failed"), pdfOk);
		return 203;
	}
	CloseHandle(pdfOkFile);

	// send "job $jobName" to network server

	//----------------------
	// Initialize Winsock
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (iResult != NO_ERROR)
	{
		printError(WSAGetLastError(), _T("Error at WSA startup."));
		return 204;
	}
	else
	{
		//----------------------
		// Create a SOCKET for connecting to server
		SOCKET connectSocket;
		connectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (connectSocket == INVALID_SOCKET)
		{
			printError(WSAGetLastError(), _T("Failed to create socket."));
			WSACleanup();
			return 205;
		}


		char pjdServerA[100];
		WideCharToMultiByte(CP_ACP, 0, pjdServer, -1, pjdServerA, sizeof(pjdServerA), NULL, NULL);

		hostent* pjdServerIp=gethostbyname(pjdServerA);
		if (pjdServerIp == NULL)
		{
			printError(WSAGetLastError(), _T("Failed to lookup server '%s'."), pjdServer);
			WSACleanup();
			return 210;
		}

		sockaddr_in clientService; 
		clientService.sin_family = AF_INET;

		clientService.sin_addr.s_addr = *(u_long *) pjdServerIp->h_addr_list[0];

		u_short pjdPortN = (u_short) _wtol(pjdPort);
		clientService.sin_port = htons( pjdPortN );

		if (connect(connectSocket, (SOCKADDR*) &clientService, sizeof(clientService) ) == SOCKET_ERROR)
		{
			int err = WSAGetLastError();

			TCHAR pjdServerIp[200];
			mbstowcs(pjdServerIp, inet_ntoa(clientService.sin_addr), sizeof(pjdServerIp));

			printError(err, _T("Failed to connect to '%s' port '%s=%d'."), pjdServerIp, pjdPort, pjdPortN);
			WSACleanup();
			return 206;
		}

		TCHAR packet[MAX_PATH+100];
		StringCbPrintf(packet, sizeof(packet), _T("job %s\n"), jobName);

		char packetA[MAX_PATH+100];
		WideCharToMultiByte(CP_ACP, 0, packet, -1, packetA, sizeof(packetA), NULL, NULL);

		printError(0, _T("sending data to pjd: '%s'."), packet);

		if (send(connectSocket, packetA, strlen(packetA), 0) == SOCKET_ERROR)
		{
			printError(WSAGetLastError(), _T("Failed to send data '%s'."), packet);
			WSACleanup();
			return 207;
		}

		if (shutdown(connectSocket, SD_SEND) == SOCKET_ERROR)
		{
			printError(WSAGetLastError(), _T("Failed to shutdown socket."));
			WSACleanup();
			return 208;
		}

		if (closesocket(connectSocket) == SOCKET_ERROR)
		{
			printError(WSAGetLastError(), _T("Failed to close socket."));
			WSACleanup();
			return 209;
		}

		WSACleanup();
	}

	return 0;
}

