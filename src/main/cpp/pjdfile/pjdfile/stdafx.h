// stdafx.h : Includedatei f�r Standardsystem-Includedateien
// oder h�ufig verwendete projektspezifische Includedateien,
// die nur in unregelm��igen Abst�nden ge�ndert werden.
//

#pragma once

#include "targetver.h"

#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <strsafe.h>
#include <io.h>
#include <fcntl.h>
#include <time.h>
#include <winsock.h>

#define GSDLLEXPORT __declspec(dllimport)
#include "Ghostscript/iapi.h"
#include "Ghostscript/ierrors.h"